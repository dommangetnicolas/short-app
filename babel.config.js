module.exports = function (api) {
  api.cache(true);
  return {
    presets: ['module:metro-react-native-babel-preset'],
    plugins: [
      [
        'module-resolver',
        {
          root: ['./'],
          extensions: ['.js'],
          alias: {
            _types: './src/@types',
            _api: './src/api',
            _assets: './src/assets',
            _components: './src/components',
            _config: './src/config',
            _constants: './src/constants',
            _context: './src/context',
            _data: './src/data',
            _enums: './src/enums',
            _environments: './src/environments',
            _functions: './src/functions',
            _helpers: './src/helpers',
            _hooks: './src/hooks',
            _layouts: './src/layouts',
            _models: './src/models',
            _navigation: './src/navigation',
            _screens: './src/screens',
            _store: './src/store',
            _tevno: './src/tevno',
            _utils: './src/utils',
          },
        },
      ],
    ],
  };
};
