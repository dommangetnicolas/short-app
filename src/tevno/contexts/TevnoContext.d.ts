import { IColors } from "../@types/IColors";
import { IFonts } from "../@types/IFonts";

export interface ITevnoContext {
  colors: IColors;
  fonts: IFonts;
}
