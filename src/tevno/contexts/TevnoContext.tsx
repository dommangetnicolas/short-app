import * as React from "react";
import { ITevnoProps } from "../@types/ITevno";
import colors from "../constants/colors";
import fonts from "../constants/fonts";
import { ITevnoContext } from "./TevnoContext.d";

const TevnoContext = React.createContext<ITevnoContext>({
  colors,
  fonts,
});

const TevnoProvider: React.FunctionComponent<ITevnoProps> = (props) => {
  const { colors: colorsFromProps, fonts: fontsFromProps } = props;

  return (
    <TevnoContext.Provider
      value={{
        colors: colorsFromProps || colors,
        fonts: fontsFromProps || fonts,
      }}
    >
      {props.children}
    </TevnoContext.Provider>
  );
};

const TevnoConsumer = TevnoContext.Consumer;

export { TevnoContext, TevnoProvider, TevnoConsumer };
