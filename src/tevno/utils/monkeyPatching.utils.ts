/**
 * String manipulation
 */
String.prototype.truncate = function (length) {
  return this.length > length ? `${this.substr(0, length - 1)}...` : this.toString();
};

String.prototype.capitalize = function () {
  return this.charAt(0).toUpperCase() + this.slice(1);
};
