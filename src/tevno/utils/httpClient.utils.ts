import { AxiosError } from "axios";

export const getHttpClientErr = (err: AxiosError) => {
  if (err?.response?.data?.message) {
    return err.response.data.message;
  }

  if (err?.response?.data) {
    return err.response.data;
  }

  if (err?.message) {
    switch (err?.message) {
      case 'Network Error':
        return 'Please try again later.';
    }

    return err?.message;
  }

  if (typeof err === 'string') {
    return err;
  }

  return 'Please try again later.';
};
