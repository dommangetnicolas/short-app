import { Color, IColors } from "./../@types/IColors";

export const getColor = (colors: IColors, color?: Color) => {
  return color ? colors[color] ?? color : undefined;
};
