const hex2rgb = (hex: string) => {
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    let c: any = hex.substring(1).split("");

    if (c.length === 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]];
    }

    c = "0x" + c.join("");

    return {
      red: (c >> 16) & 255,
      green: (c >> 8) & 255,
      blue: c & 255,
    };
  }

  return {
    red: 255,
    green: 255,
    blue: 255,
  };
};

export default hex2rgb;
