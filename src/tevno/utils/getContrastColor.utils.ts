import colors from "../constants/colors";
import { Color } from "./../@types/IColors";
import hex2rgb from "./hex2rgb.utils";

const getContrastColor = (
  hex: string,
  options?: { black?: Color | string; white?: Color | string }
) => {
  const { red, green, blue } = hex2rgb(hex);

  if (hex.toUpperCase() === "#FFB911") {
    return options?.white ?? colors.white;
  }

  if (red * 0.299 + green * 0.587 + blue * 0.114 > 186) {
    return options?.black ?? colors.black;
  }

  return options?.white ?? colors.white;
};

export default getContrastColor;
