import * as React from 'react';
import { Animated, Dimensions, StyleSheet, TouchableOpacity, View, ViewStyle } from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import SwipeableAction, { SwipeableActionWithPayload } from './components/SwipeableAction';

export type SwipeableActionPayload = false | undefined | SwipeableActionWithPayload;

export interface ISwipeableStickerProps {
  rightActions?: SwipeableActionPayload[];
  leftActions?: SwipeableActionPayload[];
  containerStyle?: ViewStyle;
  style?: ViewStyle;
  onPress?: () => void;
  onPressDisabled?: boolean;
}

const SwipeableSticker: React.FunctionComponent<ISwipeableStickerProps> = (props) => {
  const {
    rightActions: rightActionsFromProps = [],
    leftActions: leftActionsFromProps = [],
    containerStyle,
    style,
    onPress: onPressFromProps,
    onPressDisabled,
  } = props;

  const rightActions = rightActionsFromProps.filter(
    (action) => !!action,
  ) as SwipeableActionWithPayload[];
  const leftActions = leftActionsFromProps.filter(
    (action) => !!action,
  ) as SwipeableActionWithPayload[];

  const swipeableRef = React.useRef<any>();

  const onClose = () => {
    swipeableRef?.current?.close?.();
  };

  const renderActions = (
    actions: SwipeableActionWithPayload[],
    progress: Animated.AnimatedInterpolation,
  ) => {
    return (
      <View style={[styles.actionContainer, { width: (actions.length * 70) }]}>
        {actions.map((action, key) => (
          <SwipeableAction
            key={key}
            range={actions?.length - key}
            progress={progress}
            action={action}
            onClose={onClose}
          />
        ))}
      </View>
    );
  };

  const actionToggle = () => {
    const action = leftActions?.[0];
    if (!action || !action?.toggle) {
      return;
    }

    action?.toggle?.();
    onClose();
  };

  return (
    <Swipeable
      ref={swipeableRef}
      containerStyle={[styles.container, containerStyle]}
      onSwipeableLeftOpen={actionToggle}
      {...(!!leftActions.length && {
        renderLeftActions: (progress) => renderActions(leftActions, progress),
      })}
      {...(!!rightActions.length && {
        renderRightActions: (progress) => renderActions(rightActions, progress),
      })}
    >
      <TouchableOpacity
        style={style}
        disabled={onPressDisabled}
        onPress={onPressFromProps}
      >
        {props.children}
      </TouchableOpacity>
    </Swipeable>
  );
};

export default SwipeableSticker;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  actionContainer: {
    flexDirection: 'row',
  },
});
