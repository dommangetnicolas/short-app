import * as React from 'react';
import { Animated, StyleSheet, TouchableOpacity } from 'react-native';
import Spinner from '_tevno/components/Spinner';
import useTevno from '_tevno/hooks/useTevno';
import { IColors } from '../../../@types/IColors';
import { IconNames } from '../../../@types/Icon';
import { IFonts } from '../../../@types/IFonts';
import Icon, { IIconProps } from '../../Icon';

export type SwipeableActionWithPayload = {
  icon?: IconNames;
  iconProps?: Omit<IIconProps, 'name'>;
  closeOnPress?: boolean;
  isPending?: boolean;
  showIsPending?: boolean;
  onPress?: () => void;
  toggle?: () => void;
};

interface IProps {
  range: number;
  progress: Animated.AnimatedInterpolation;
  action: SwipeableActionWithPayload;
  onClose?: () => void;
}

const SwipeableAction: React.FunctionComponent<IProps> = (props) => {
  const [isPending, setIsPending] = React.useState<boolean>(false);

  const { range, progress, action, onClose } = props;
  const {
    icon,
    iconProps,
    closeOnPress = true,
    isPending: isPendingFromProps = false,
    showIsPending = true,
    onPress: onPressFromProps,
  } = action;

  React.useEffect(() => {
    setIsPending(isPendingFromProps);
  }, [isPendingFromProps]);

  const onPress = async () => {
    showIsPending && setIsPending(true);
    await onPressFromProps?.();
    showIsPending && setIsPending(false);
  };

  const trans = progress.interpolate({
    inputRange: [0, 1],
    outputRange: [range * 64, 0],
  });

  const { colors, fonts } = useTevno();
  const styles = getStyles(colors, fonts);

  return (
    <Animated.View key={range} style={[styles.action, { transform: [{ translateX: trans }] }]}>
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          onPress?.();
          closeOnPress && onClose?.();
        }}
      >
        {!isPending && <Icon name={icon ?? 'add'} size={24} color={colors.brand} {...iconProps} />}

        {isPending && <Spinner size={24} />}
      </TouchableOpacity>
    </Animated.View>
  );
};

export default SwipeableAction;

const getStyles = (colors: IColors, fonts: IFonts) =>
  StyleSheet.create({
    action: {
      flex: 1,
      fontFamily: fonts.primaryBold,
      justifyContent: 'center',
    },
    button: {
      height: 60,
      width: 60,
      borderRadius: 60,
      backgroundColor: colors.white,
      alignItems: 'center',
      justifyContent: 'center',

      shadowColor: colors.greyDarker,
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,

      elevation: 3,
    },
  });
