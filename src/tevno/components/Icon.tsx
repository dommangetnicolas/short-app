import * as React from "react";
import { StyleProp, TextStyle } from "react-native";
import IconComponent from "react-native-vector-icons/Ionicons";
import { IconNames } from "../@types/Icon";
import Badge, { IBadgeProps } from "./Badge";

export interface IIconProps {
  name: IconNames;
  size?: number;
  color?: string;
  style?: StyleProp<TextStyle>;
  badge?: IBadgeProps;
}

const Icon: React.FunctionComponent<IIconProps> = ({
  name,
  size,
  color,
  style,
  badge,
}) => {
  const result: never = name as never;

  return (
    <>
      <IconComponent style={style} name={result} size={size} color={color} />
      {badge && <Badge {...badge} />}
    </>
  );
};

export default Icon;
