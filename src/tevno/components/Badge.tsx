import * as React from "react";
import { StyleSheet, Text, View } from "react-native";
import { IColors } from "../@types/IColors";
import { IFonts } from "../@types/IFonts";
import useTevno from "../hooks/useTevno";

export interface IBadgePosition {
  top?: number;
  right?: number;
  bottom?: number;
  left?: number;
}

export interface IBadgeProps {
  value?: number;
  mini?: boolean;
  color?: string;
  position?: IBadgePosition;
}

const Badge: React.FunctionComponent<IBadgeProps> = (props) => {
  const { mini, value, color, position } = props;

  const { colors, fonts } = useTevno();
  const styles = getStyles(colors, fonts, position, color);

  return (
    <View style={styles.container}>
      {mini && <View style={[styles.badge, styles.miniBadge]} />}

      {value !== undefined && value > 0 && (
        <View style={styles.badge}>
          <Text style={styles.badgeText}>{value < 9 ? value : "9+"}</Text>
        </View>
      )}
    </View>
  );
};

export default Badge;

const size = 18;
const miniSize = 9;

const getStyles = (
  colors: IColors,
  fonts: IFonts,
  position?: IBadgePosition,
  color?: string
) =>
  StyleSheet.create({
    container: {
      position: "absolute",
      zIndex: 3,
      top: -8,
      right: -5,
      ...position,
    },
    badge: {
      alignSelf: "center",
      minWidth: size,
      height: size,
      borderRadius: size / 2,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: color ? color : colors.brand,
      borderWidth: StyleSheet.hairlineWidth,
      borderColor: "#fff",
    },
    miniBadge: {
      paddingHorizontal: 0,
      paddingVertical: 0,
      minWidth: miniSize,
      height: miniSize,
      borderRadius: miniSize / 2,
    },
    badgeText: {
      color: colors.white,
      fontFamily: fonts.primarySemiBold,
      fontSize: 12,
    },
  });
