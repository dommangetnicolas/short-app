import LottieView from "lottie-react-native";
import * as React from "react";
import { Animated, StyleProp, ViewStyle } from "react-native";
import LottieSpinnerBrand from "../assets/animations/lottie-spinner-brand.json";
import LottieSpinnerWhite from "../assets/animations/lottie-spinner-white.json";

export interface ISpinnerProps {
  color?: "white" | "brand";
  style?: StyleProp<ViewStyle>;
  size?: number;
}

const Spinner: React.FunctionComponent<ISpinnerProps> = (props) => {
  const { color, size = 22, style } = props;

  const progress = React.useRef(new Animated.Value(0)).current;

  const AnimateFunction = () => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(progress, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
        }),
        Animated.timing(progress, {
          toValue: 0,
          duration: 1000,
          useNativeDriver: true,
        }),
      ])
    ).start();
  };

  React.useEffect(() => {
    AnimateFunction();
  });

  return (
    <LottieView
      source={color === "white" ? LottieSpinnerWhite : LottieSpinnerBrand}
      autoPlay
      loop
      style={[{ height: size, width: size }, style]}
      progress={progress}
    />
  );
};

export default Spinner;
