import * as React from "react";
import {
  SafeAreaView,
  StatusBarProps,
  StyleProp,
  StyleSheet,
  View,
  ViewProps,
  ViewStyle,
} from "react-native";
import useTevno from "_tevno/hooks/useTevno";
import FocusAwareStatusBar from "./FocusAwareStatusBar";

export interface IScreenProps extends ViewProps {
  isSafe?: boolean;
  isBottomSafe?: boolean;
  safeAreaViewStyle?: StyleProp<ViewStyle>;
  focusAwareStatusBarProps?: StatusBarProps;
}

const Screen: React.FunctionComponent<IScreenProps> = (props) => {
  const {
    isSafe = true,
    isBottomSafe = true,
    children,
    style,
    safeAreaViewStyle,
    focusAwareStatusBarProps,
  } = props;

  const { colors } = useTevno();

  return (
    <View
      style={[styles.container, { backgroundColor: colors.background }, style]}
    >
      <FocusAwareStatusBar
        barStyle="dark-content"
        backgroundColor={colors.background}
        {...focusAwareStatusBarProps}
      />

      {isSafe && (
        <SafeAreaView
          style={[{ backgroundColor: colors.background }, safeAreaViewStyle]}
        />
      )}

      {children}

      {isBottomSafe && (
        <SafeAreaView
          style={[{ backgroundColor: colors.background }, safeAreaViewStyle]}
        />
      )}
    </View>
  );
};

export default Screen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
