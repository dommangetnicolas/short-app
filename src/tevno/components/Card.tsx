import * as React from 'react';
import {
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  TouchableOpacityProps,
  View,
  ViewStyle,
} from 'react-native';
import LinearGradient, { LinearGradientProps } from 'react-native-linear-gradient';
import { IColors } from '_tevno/@types/IColors';
import useTevno from '_tevno/hooks/useTevno';

export interface ICardProps extends Omit<TouchableOpacityProps, 'style'> {
  containerStyle?: StyleProp<ViewStyle>;
  gradientStyle?: StyleProp<ViewStyle>;
  bodyStyle?: StyleProp<ViewStyle>;

  gradientProps?: LinearGradientProps;
}

const Card: React.FunctionComponent<ICardProps> = (props) => {
  const {
    children,
    onPress,

    containerStyle,
    gradientStyle,
    bodyStyle,

    gradientProps,
  } = props;

  const { colors } = useTevno();
  const styles = getStyles(colors);

  const shadowColor = gradientProps?.colors?.[0]?.toString() ?? colors.greyDark;
  const biggerShadow = {
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  };

  return (
    <TouchableOpacity
      {...props}
      style={[
        styles.container,
        {
          shadowColor,
          ...(shadowColor !== colors.greyDark && { ...biggerShadow }),
        },
        containerStyle,
      ]}
      disabled={!onPress}
    >
      <LinearGradient
        style={[styles.gradientContainer, gradientStyle]}
        start={{ x: 0, y: 0.5 }}
        end={{ x: 1, y: 0.5 }}
        colors={[colors.white, colors.white]}
        {...gradientProps}
      >
        <View style={[styles.body, bodyStyle]}>{children}</View>
      </LinearGradient>
    </TouchableOpacity>
  );
};

export default Card;

const getStyles = (colors: IColors) =>
  StyleSheet.create({
    container: {
      borderRadius: 10,

      shadowColor: colors.greyDarker,
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,

      elevation: 3,
    },
    gradientContainer: {
      borderRadius: 10,
    },
    header: {
      flexDirection: 'row',
      alignItems: 'center',
      padding: 15,
    },
    headerTitle: {
      flexWrap: 'wrap',
      flex: 1,
    },
    headerIcon: {
      marginRight: 15,
      fontSize: 22,
    },
    headerLine: {
      marginTop: 0,
    },
    body: {
      padding: 15,
    },
  });
