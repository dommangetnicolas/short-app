import * as React from 'react';
import {
  StyleProp,
  StyleSheet,
  TextStyle,
  TouchableOpacity,
  TouchableOpacityProps,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import { IconNames } from '../@types/Icon';
import useTevno from '../hooks/useTevno';
import { IAnimation } from '../@types/IAnimation';
import { Color, IColors } from '../@types/IColors';
import { IFonts } from '../@types/IFonts';
import { removeMargin, removePadding } from '../constants/styles';
import getContrastColor from '../utils/getContrastColor.utils';
import hex2rgba from '../utils/hex2rgba.utils';
import Icon from './Icon';
import Spinner from './Spinner';
import Text from './Text';

export interface IButtonProps extends TouchableOpacityProps {
  disabled?: boolean;
  isPending?: boolean;
  outline?: boolean;
  nude?: boolean;
  round?: boolean;
  block?: boolean;
  prefixIcon?: IconNames;
  prefixIconProps?: Omit<IconNames, 'name'>;
  icon?: IconNames;
  iconProps?: Omit<IconNames, 'name'>;
  suffixIcon?: IconNames;
  suffixIconProps?: Omit<IconNames, 'name'>;
  color?: Color;
  size?: 'md' | 'lg';

  textStyle?: StyleProp<TextStyle>;
  animation?: IAnimation;
}

const Button: React.FunctionComponent<IButtonProps> = (props) => {
  const {
    disabled,
    isPending,
    outline,
    nude,
    block,
    round,
    prefixIcon,
    prefixIconProps,
    icon,
    iconProps,
    suffixIcon,
    suffixIconProps,
    color: colorFromProps,
    size = 'md',

    style,
    textStyle,
    children,
    animation,
  } = props;

  const { colors, fonts } = useTevno();

  let color: string | undefined;
  let textColor = colors.greyDarker;

  if (colorFromProps) {
    color = colors[colorFromProps] ?? colorFromProps;
  }

  textColor = color ? getContrastColor(color) : colors.greyDark;

  if (nude) {
    textColor = color ?? colors.greyDarker;
    color = 'transparent';
  }
  if (color && outline) {
    textColor = color;
  }

  const shadowColor = color ?? colors.greyDark;
  const biggerShadow = {
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  };

  const styles = getStyles(colors, fonts, color, textColor);

  return (
    <Animatable.View {...animation} style={[style, removePadding]}>
      <TouchableOpacity
        disabled={disabled ?? isPending}
        {...props}
        style={[
          styles.container,
          {
            shadowColor,
            ...(shadowColor !== colors.greyDark && { ...biggerShadow }),
          },
          disabled && styles.containerDisabled,
          outline && styles.containerOutline,
          nude && styles.containerNude,
          block && styles.containerBlock,
          (prefixIcon || isPending) && styles.containerWithPrefix,
          suffixIcon && !isPending && styles.containerWithSuffix,
          round && styles.containerRound,
          round && icon && styles.containerRoundWithIcon,
          size === 'md' && styles.containerMd,
          size === 'lg' && styles.containerLg,
          style,
          { flex: 0 },
          removeMargin,
        ]}
      >
        {isPending && (
          <Spinner
            style={[styles.loader, (icon || round) && styles.loaderWithIcon]}
            color={textColor === '#FFFFFF' ? 'white' : 'brand'}
          />
        )}

        {!isPending && prefixIcon && (
          <Icon
            style={styles.prefixIcon}
            color={textColor}
            name={prefixIcon}
            {...prefixIconProps}
          />
        )}

        {!icon && typeof children === 'string' && (
          <Text style={[styles.text, textStyle]}>{children}</Text>
        )}

        {!isPending && icon && (
          <Icon style={styles.icon} color={textColor} name={icon} {...iconProps} />
        )}

        {typeof children !== 'string' && children}

        {!isPending && suffixIcon && (
          <Icon
            style={styles.suffixIcon}
            color={textColor}
            name={suffixIcon}
            {...suffixIconProps}
          />
        )}
      </TouchableOpacity>
    </Animatable.View>
  );
};

export default Button;

const getStyles = (colors: IColors, fonts: IFonts, color?: string, textColor?: string) =>
  StyleSheet.create({
    container: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      paddingVertical: 10,
      paddingHorizontal: 25,
      borderRadius: 10,
      backgroundColor: color ?? colors.white,
      alignSelf: 'flex-start',

      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,

      elevation: 3,
    },
    containerMd: {
      minHeight: 50,
    },
    containerLg: {
      minHeight: 60,
    },
    containerDisabled: {},
    containerOutline: {
      backgroundColor: hex2rgba(color ?? colors.greyDarker, 0.15),

      shadowOpacity: 0,
      elevation: 0,
    },
    containerNude: {
      shadowOpacity: 0,
      elevation: 0,
    },
    containerBlock: {
      alignSelf: undefined,
    },
    containerWithPrefix: {
      paddingLeft: 15,
    },
    containerWithSuffix: {
      paddingRight: 15,
    },
    containerRound: {
      borderRadius: 1000,
    },
    containerRoundWithIcon: {
      minHeight: 50,
      aspectRatio: 1,
    },
    text: {
      textTransform: 'uppercase',
      fontSize: 16,
      fontFamily: fonts.primarySemiBold,
      color: textColor,
    },
    loader: {
      marginRight: 15,
    },
    icon: {
      fontSize: 20,
      position: 'absolute',
    },
    loaderWithIcon: {
      position: 'absolute',
    },
    prefixIcon: {
      marginRight: 15,
      fontSize: 20,
    },
    suffixIcon: {
      marginLeft: 15,
      fontSize: 20,
    },
  });
