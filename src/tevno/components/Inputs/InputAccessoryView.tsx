import * as React from 'react';
import {
  InputAccessoryView,
  InputAccessoryViewProps,
  Keyboard,
  Platform,
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import useTevno from '_tevno/hooks/useTevno';
import { IColors } from '../../@types/IColors';

export interface IInputAccessoryViewProps extends InputAccessoryViewProps {
  contentContainerStyle?: StyleProp<ViewStyle>;
  doneText?: string;
  onDone?: () => void;
}

const CustomInputAccessoryView: React.FunctionComponent<IInputAccessoryViewProps> = (props) => {
  const { style, contentContainerStyle, doneText, onDone } = props;

  const { colors } = useTevno();

  if (Platform.OS === 'android') {
    return null;
  }

  const styles = getStyles(colors);

  return (
    <InputAccessoryView {...props} style={[style]}>
      <View style={[styles.contentContainer, contentContainerStyle]}>
        <TouchableOpacity onPress={onDone ?? Keyboard.dismiss}>
          <Text style={styles.doneText}>{doneText ?? 'Confirmer'}</Text>
        </TouchableOpacity>
      </View>
    </InputAccessoryView>
  );
};

export default CustomInputAccessoryView;

const getStyles = (colors: IColors) =>
  StyleSheet.create({
    contentContainer: {
      height: 45,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
      backgroundColor: '#f8f8f8',
      borderTopWidth: 1,
      borderTopColor: '#dedede',
      zIndex: 2,
    },
    doneText: {
      color: colors.brand,
      fontWeight: '600',
      fontSize: 17,
      paddingTop: 1,
      paddingRight: 20,
    },
  });
