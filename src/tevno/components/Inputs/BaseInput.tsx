/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import { FunctionComponent } from 'react';
import {
  Animated,
  NativeSyntheticEvent,
  StyleProp,
  StyleSheet,
  TextInputFocusEventData,
  View,
  ViewStyle,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { IconNames } from '_tevno/@types/Icon';
import useTevno from '_tevno/hooks/useTevno';
import hex2rgba from '_tevno/utils/hex2rgba.utils';
import { IAnimation } from '../../@types/IAnimation';
import { Color, IColors } from '../../@types/IColors';
import { IFonts } from '../../@types/IFonts';
import Icon, { IIconProps } from '../Icon';
import Text from '../Text';

interface IconInputProps extends Omit<IIconProps, 'name'> {
  onPress?: () => void;
}

export interface IBaseInputProps {
  style?: StyleProp<ViewStyle>;
  animation?: IAnimation;
  required?: boolean;
  disabled?: boolean;
  isInvalid?: boolean;
  isValid?: boolean;

  placeholder?: string;
  value?: any;
  prefixIcon?: IconNames;
  suffixIcon?: IconNames;
  prefixIconProps?: IconInputProps;
  suffixIconProps?: IconInputProps;

  onFocus?: (e: NativeSyntheticEvent<TextInputFocusEventData>) => void;
  onBlur?: (e: NativeSyntheticEvent<TextInputFocusEventData>) => void;
}

const BaseInput: FunctionComponent<IBaseInputProps> = (props) => {
  const {
    style,
    animation,
    placeholder,
    value,
    disabled,
    required,
    isInvalid,
    isValid,
    prefixIcon,
    suffixIcon,
    prefixIconProps,
    suffixIconProps,
    onFocus: onFocusFromProps,
    onBlur: onBlurFromProps,
    children,
  } = props;
  const [isFocused, setIsFocused] = React.useState(false);

  const labelOpacity = React.useRef(new Animated.Value(0)).current;
  const { colors, fonts } = useTevno();

  let borderColor = colors.greyLight;

  if (isFocused) {
    borderColor = colors.info;
  }

  if (isValid) {
    borderColor = colors.success;
  }

  if (isInvalid) {
    borderColor = colors.danger;
  }

  const styles = getStyles(colors, fonts, StyleSheet.flatten(style), borderColor);

  const onFocus = (e: NativeSyntheticEvent<TextInputFocusEventData>) => {
    setIsFocused(true);

    Animated.timing(labelOpacity, {
      toValue: 1,
      duration: 150,
      useNativeDriver: true,
    }).start();

    onFocusFromProps?.(e);
  };

  const onBlur = (e: NativeSyntheticEvent<TextInputFocusEventData>) => {
    setIsFocused(false);

    Animated.timing(labelOpacity, {
      toValue: 0,
      duration: 150,
      useNativeDriver: true,
    }).start();

    onBlurFromProps?.(e);
  };

  return (
    <View style={styles.upperContainer}>
      {!!placeholder && (
        <Animated.Text style={styles.label}>
          {placeholder}
          {required && <Text style={styles.required}>*</Text>}
        </Animated.Text>
      )}

      <Animatable.View
        style={[styles.container, disabled && styles.containerDisabled]}
        {...animation}
      >
        {prefixIcon && (
          <TouchableOpacity onPress={prefixIconProps?.onPress} disabled={!prefixIconProps?.onPress}>
            <Icon
              style={styles.prefixIcon}
              color={borderColor}
              name={prefixIcon}
              {...prefixIconProps}
            />
          </TouchableOpacity>
        )}

        {React.cloneElement(children as React.ReactElement, {
          onBlur,
          onFocus,
          disabled,
        })}

        {suffixIcon && (
          <TouchableOpacity onPress={suffixIconProps?.onPress} disabled={!suffixIconProps?.onPress}>
            <Icon
              style={styles.suffixIcon}
              color={borderColor}
              name={suffixIcon}
              {...suffixIconProps}
            />
          </TouchableOpacity>
        )}
      </Animatable.View>
    </View>
  );
};

export default BaseInput;

const getStyles = (colors: IColors, fonts: IFonts, style?: ViewStyle, borderColor?: string) =>
  StyleSheet.create({
    upperContainer: {
      flex: style?.flex,
      minWidth: style?.minWidth,

      margin: style?.margin,
      marginVertical: style?.marginVertical,
      marginHorizontal: style?.marginHorizontal,
      marginTop: style?.marginTop,
      marginBottom: style?.marginBottom,
      marginLeft: style?.marginLeft,
      marginRight: style?.marginRight,
    },
    container: {
      flex: undefined,
      flexDirection: 'row',
      alignItems: 'center',

      borderWidth: style?.borderWidth ?? 2,
      borderRightWidth: style?.borderRightWidth ?? 2,
      borderLeftWidth: style?.borderLeftWidth ?? 2,
      borderTopWidth: style?.borderTopWidth ?? 2,
      borderBottomWidth: style?.borderBottomWidth ?? 2,

      borderColor: borderColor ?? colors.greyLight,
      borderRadius: style?.borderRadius ?? 5,
      borderTopRightRadius: style?.borderTopRightRadius ?? 5,
      borderTopLeftRadius: style?.borderTopLeftRadius ?? 5,
      borderBottomRightRadius: style?.borderBottomRightRadius ?? 5,
      borderBottomLeftRadius: style?.borderBottomLeftRadius ?? 5,
    },
    containerDisabled: {
      backgroundColor: colors.greyLighter,
    },
    label: {
      fontSize: 14,
      fontFamily: fonts.primaryRegular,
      color: hex2rgba(colors.greyDarker, 0.6),

      paddingHorizontal: 5,
      marginBottom: 5,
      zIndex: 1,
    },
    required: {
      color: colors.danger,
      fontSize: 12,
    },
    prefixIcon: {
      marginLeft: 15,
      fontSize: 15,
    },
    suffixIcon: {
      paddingRight: 15,
      fontSize: 15,
    },
  });
