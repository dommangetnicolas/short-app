import * as React from "react";
import {
  Platform,
  StyleSheet,
  TextInput,
  TextInputProps,
  ViewStyle,
} from "react-native";
import useTevno from "_tevno/hooks/useTevno";
import { IColors } from "../../@types/IColors";
import BaseInput, { IBaseInputProps } from "./BaseInput";
import CustomInputAccessoryView, {
  IInputAccessoryViewProps,
} from "./InputAccessoryView";

export interface ITextInputProps
  extends Omit<IBaseInputProps, "value">,
    Omit<TextInputProps, "style"> {
  value?: string;
  isPassword?: boolean;
  doneText?: string;
  showDone?: boolean;
  inputAccessoryViewProps?: IInputAccessoryViewProps;
  textArea?: boolean | number;
  oneChar?: boolean;
}

const CustomTextInput = React.forwardRef((props: ITextInputProps, ref: any) => {
  const {
    isPassword,
    doneText,
    showDone: showDoneFromProps,
    style,
    placeholder,
    keyboardType,
    returnKeyType,
    disabled,
    onSubmitEditing,
    inputAccessoryViewProps,
    isInvalid,
    textArea,
    oneChar,
  } = props;
  const [isSecure, setIsSecure] = React.useState(isPassword);

  const { colors } = useTevno();
  const styles = getStyles(colors, StyleSheet.flatten(style));

  const randomId = Math.floor(Math.random() * 10001);

  const defaultDone =
    (keyboardType === "number-pad" ||
      keyboardType === "decimal-pad" ||
      keyboardType === "phone-pad") &&
    (returnKeyType === "done" || returnKeyType === "next");
  const showDone =
    showDoneFromProps !== undefined ? showDoneFromProps : defaultDone;

  return (
    <BaseInput
      {...props}
      {...(isPassword && {
        suffixIconProps: {
          onPress: () => setIsSecure(!isSecure),
          color: isInvalid ? colors.danger : colors.greyDark,
        },
        suffixIcon: isSecure ? "eye-outline" : "eye-off-outline",
      })}
      style={[styles.oneCharBaseInput, style]}
    >
      <>
        <TextInput
          placeholderTextColor={colors.grey}
          multiline={!!textArea}
          {...props}
          ref={ref}
          secureTextEntry={isSecure}
          style={[
            styles.textInput,
            !!textArea && styles.textArea,
            oneChar && styles.oneChar,
            typeof textArea === "number" && { minHeight: textArea },
          ]}
          editable={!disabled}
          {...(Platform.OS === "ios" &&
            showDone && {
              inputAccessoryViewID: `${placeholder}${randomId}`,
            })}
        />
        {Platform.OS === "ios" && showDone && (
          <CustomInputAccessoryView
            nativeID={`${placeholder}${randomId}`}
            onDone={() => onSubmitEditing?.(null as any)}
            doneText={doneText}
            {...inputAccessoryViewProps}
          />
        )}
      </>
    </BaseInput>
  );
});

export default CustomTextInput;

const getStyles = (colors: IColors, style?: ViewStyle) =>
  StyleSheet.create({
    textInput: {
      flex: 1,
      paddingVertical: 10,
      paddingHorizontal: 15,
      borderRadius: 5,

      fontSize: 14,
      color: colors.greyDarker,

      ...style,

      margin: undefined,
      marginVertical: undefined,
      marginHorizontal: undefined,
      marginTop: undefined,
      marginBottom: undefined,
      marginLeft: undefined,
      marginRight: undefined,
    },
    textArea: {
      paddingTop: 10,
      minHeight: 60,
      textAlignVertical: "top",
    },
    oneCharBaseInput: {
      minWidth: 50,
    },
    oneChar: {
      textAlign: "center",
    },
  });
