import * as React from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";

export interface IFlatListFooterProps {
  isFetchingMore: boolean;
}

const FlatListFooter: React.FunctionComponent<IFlatListFooterProps> = (
  props
) => {
  const { isFetchingMore } = props;

  if (!isFetchingMore) {
    return null;
  }

  return (
    <View style={styles.container}>
      <ActivityIndicator />
    </View>
  );
};

export default FlatListFooter;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
  },
});
