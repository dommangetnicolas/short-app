import * as React from 'react';
import { StyleSheet, TextProps } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { IAnimation } from '../@types/IAnimation';
import { Color } from '../@types/IColors';
import { IFonts, Weight } from '../@types/IFonts';
import useTevno from '../hooks/useTevno';
import { getColor } from '../utils/getColor.utils';

const weights = {
  light: 'Raleway-Light',
  regular: 'Raleway-Medium',
  semiBold: 'Raleway-SemiBold',
  bold: 'Raleway-Bold',
};

export interface ITextProps extends TextProps {
  weight?: Weight;
  color?: Color;
  animation?: IAnimation;
}

const CustomText: React.FunctionComponent<ITextProps> = (props) => {
  const {
    children,
    style,
    weight = 'regular',
    animation,
    color: colorFromProps = 'greyDarker',
  } = props;

  const { fonts, colors } = useTevno();
  const styles = getStyles(fonts);

  let color = getColor(colors, colorFromProps);

  return (
    <Animatable.Text
      {...props}
      animation={undefined}
      {...animation}
      style={[styles.text, { fontFamily: weights[weight], color }, style]}
    >
      {children}
    </Animatable.Text>
  );
};

export default CustomText;

const getStyles = (fonts: IFonts) =>
  StyleSheet.create({
    text: {
      fontSize: 16,
      fontFamily: fonts.primaryRegular,
    },
  });
