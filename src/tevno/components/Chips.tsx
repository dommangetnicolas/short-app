import * as React from 'react';
import { Platform, StyleSheet, TouchableOpacity, View, ViewProps } from 'react-native';
import useTevno from '_tevno/hooks/useTevno';
import { Color, IColors } from '../@types/IColors';
import { IFonts } from '../@types/IFonts';
import getContrastColor from '../utils/getContrastColor.utils';
import hex2rgba from '../utils/hex2rgba.utils';
import Text from './Text';

export interface IChipsProps extends ViewProps {
  outline?: boolean;
  color?: Color;
  onClose?: () => void;
}

const Chips: React.FunctionComponent<IChipsProps> = (props) => {
  const { style, color: colorFromProps, outline, onClose, children } = props;

  const { colors, fonts } = useTevno();

  let color: string | undefined;
  let textColor = colors.greyDarker;

  if (colorFromProps) {
    color = colors[colorFromProps] ?? colorFromProps;
  }

  textColor = color ? getContrastColor(color) : colors.greyDark;

  if (color && outline) {
    textColor = color;
  }

  const styles = getStyles(colors, fonts, color, textColor);

  const renderFull = (child: any) => {
    return (
      <View style={[styles.containerFull, outline && styles.backgroundOutline]}>
        {child}

        {onClose && (
          <TouchableOpacity style={styles.crossContainer} onPress={onClose}>
            <Text style={styles.cross}>&times;</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  };

  const renderRight = (child: any) => {
    return (
      <View style={[styles.containerRight, outline && styles.borderOutline]}>
        {child}

        {onClose && (
          <TouchableOpacity style={styles.crossContainer} onPress={onClose}>
            <Text style={[styles.cross, styles.crossRight]}>&times;</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  };

  const renderLeft = (child: any) => {
    return <View style={[styles.containerLeft, outline && styles.backgroundOutline]}>{child}</View>;
  };

  const getComponents = () => {
    if (children && typeof children === 'object' && (children as any)?.length) {
      return React.Children.map(children, (child: any, key) => {
        const childStyle = [styles.text, key === 1 && styles.textRight, child?.props?.style];
        let component: any;

        if (child.type === Text) {
          component = React.cloneElement(child as React.ReactElement, {
            style: childStyle,
          });
        }

        if (child.type !== Text) {
          component = <Text style={childStyle}>{children}</Text>;
        }

        return key === 0 ? renderLeft(component) : renderRight(component);
      });
    }

    return renderFull(<Text style={styles.text}>{children}</Text>);
  };

  return <View style={[styles.container, style]}>{getComponents()}</View>;
};

export default Chips;

const getStyles = (colors: IColors, fonts: IFonts, color?: string, textColor?: string) =>
  StyleSheet.create({
    container: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      alignSelf: 'flex-start',
    },

    backgroundOutline: {
      backgroundColor: hex2rgba(color ?? colors.greyDark, 0.15),
      borderColor: hex2rgba(color ?? colors.greyDark, Platform.OS === 'android' ? 0.15 : 0.01),
    },
    borderOutline: {
      borderColor: hex2rgba(color ?? colors.greyDark, 0.15),
    },

    containerFull: {
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: color ?? colors.white,
      paddingVertical: 2,
      paddingHorizontal: 7,
      borderColor: color ?? colors.white,
      borderWidth: 2,
      borderRadius: 1000,
    },
    containerRight: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingTop: 2,
      paddingBottom: 2,
      paddingHorizontal: 7,
      paddingLeft: 5,
      borderColor: color ?? colors.white,
      borderWidth: 2,
      borderTopRightRadius: 1000,
      borderBottomRightRadius: 1000,
    },
    containerLeft: {
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: color ?? colors.white,
      paddingVertical: 2,
      paddingHorizontal: 7,
      paddingRight: 5,
      borderColor: color ?? colors.white,
      borderWidth: 2,
      borderTopLeftRadius: 1000,
      borderBottomLeftRadius: 1000,
    },

    text: {
      fontFamily: fonts.primarySemiBold,
      color: textColor,
      fontSize: 14,
      lineHeight: 18,
    },
    textRight: {
      color: color ?? colors.greyDark,
      lineHeight: 18,
    },

    crossContainer: {
      alignItems: 'flex-end',
      marginLeft: 10,
      width: 10,
      height: 18,
    },
    cross: {
      color: textColor,
      fontSize: 16,
      lineHeight: Platform.OS === 'android' ? 18 : 17,
      fontFamily: fonts.primarySemiBold,
    },
    crossRight: {
      color: color ?? colors.greyDark,
      lineHeight: Platform.OS === 'android' ? 18 : 16,
    },
  });
