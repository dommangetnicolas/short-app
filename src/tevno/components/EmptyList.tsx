import * as React from "react";
import { RefreshControl, ScrollView, StyleSheet, View } from "react-native";
import useTevno from "_tevno/hooks/useTevno";
import { IColors } from "../@types/IColors";
import { IFonts } from "../@types/IFonts";
import Text from "./Text";

export interface IEmptyListProps {
  isFetching: boolean;
  isFetchingMore?: boolean;
  onRefresh: () => void;
  text?: string;
  ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null;
}

const EmptyList: React.FunctionComponent<IEmptyListProps> = (props) => {
  const {
    isFetching,
    isFetchingMore,
    onRefresh,
    text = "No data",
    ListHeaderComponent,
  } = props;
  const { colors, fonts } = useTevno();
  const styles = getStyles(colors, fonts);

  return (
    <ScrollView
      style={styles.scrollView}
      contentContainerStyle={styles.scrollView}
      refreshControl={
        <RefreshControl
          refreshing={isFetching && !isFetchingMore}
          onRefresh={onRefresh}
        />
      }
    >
      {ListHeaderComponent && ListHeaderComponent}
      <View style={styles.container}>
        <Text style={styles.noDataText}>{text}</Text>
      </View>
    </ScrollView>
  );
};

export default EmptyList;

const getStyles = (colors: IColors, fonts: IFonts) =>
  StyleSheet.create({
    scrollView: {
      flex: 1,
      backgroundColor: colors.background,
    },
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
    noDataText: {
      fontSize: 18,
      fontFamily: fonts.primaryBold,
      color: colors.greyDark,
    },
  });
