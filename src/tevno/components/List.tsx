import { useNavigation } from '@react-navigation/native';
import { AxiosResponse } from 'axios';
import * as React from 'react';
import { Alert, FlatList, FlatListProps, RefreshControl, StyleSheet } from 'react-native';
import * as Animatable from 'react-native-animatable';
import useTevno from '_tevno/hooks/useTevno';
import { getHttpClientErr } from '_tevno/utils/httpClient.utils';
import { IListItemBase } from '_types/api/IListItemBase';
import { IListResponse } from '_types/api/IListResponse';
import { IColors } from '../@types/IColors';
import EmptyList, { IEmptyListProps } from './EmptyList';
import FlatListFooter from './FlatListFooter';

interface IExtendedEmptyListProps extends Omit<Omit<IEmptyListProps, 'isFetching'>, 'onRefresh'> {
  isFetching?: boolean;
  isFetchingMore?: boolean;
  onRefresh?: () => void;
  text?: string;
}

export interface IListProps<Item> extends Omit<Omit<FlatListProps<Item>, 'data'>, 'renderItem'> {
  fetch: (page: number) => Promise<AxiosResponse<IListResponse<Item>>>;
  renderItem: ({
    item,
    onRefresh,
    onSelect,
    isSelected,
  }: {
    item: Item;
    onRefresh: () => void;
    onSelect?: (id: number) => void;
    isSelected?: boolean;
  }) => JSX.Element;

  innerRef?: any;
  animationOnItem?: boolean;
  selectedIds?: Array<Number>;
  setSelectedIds?: (values: Array<number> | ((prevValues: Array<number>) => Array<number>)) => void;

  emptyListProps?: IExtendedEmptyListProps;
}

const List = <Item extends IListItemBase>(props: IListProps<Item>) => {
  const [isEmpty, setIsEmpty] = React.useState(false);
  const [isFetching, setIsFetching] = React.useState(false);
  const [isFetchingMore, setIsFetchingMore] = React.useState(false);
  const [isNoMore, setIsNoMore] = React.useState(false);
  const [data, setData] = React.useState<Array<any>>([]);
  const [loadedPages, setLoadedPages] = React.useState<Array<number>>([]);
  const [page, setPage] = React.useState(1);

  const {
    fetch: fetchFromProps,
    renderItem: renderItemFromProps,
    innerRef,
    animationOnItem = true,
    selectedIds,
    setSelectedIds,
    emptyListProps,
  } = props;

  const { addListener } = useNavigation();
  const { colors } = useTevno();

  const fetch = React.useCallback(async () => {
    try {
      if (!loadedPages.includes(page)) {
        const {
          data: { totalDocs, docs },
        } = await fetchFromProps(page);
        const newData = page === 1 ? docs : [...data, ...docs];

        setLoadedPages((ld) => [...ld, page]);
        setData(newData);
        setIsNoMore(totalDocs === newData.length || totalDocs < 10);
        setIsEmpty(!totalDocs);
      }
    } catch (err) {
      Alert.alert('Oops, an error has occured', getHttpClientErr(err));
    } finally {
      setIsFetching(false);
      setIsFetchingMore(false);
    }
  }, [data, fetchFromProps, loadedPages, page]);

  const onLoadMore = () => {
    if (!isFetching && !isFetchingMore && !isNoMore) {
      setPage(page + 1);
      setIsFetchingMore(true);
    }
  };

  const onRefresh = React.useCallback(() => {
    setIsFetching(true);
    setLoadedPages([]);
    setPage(1);
  }, []);

  const onSelect = (id: number) => {
    if (!selectedIds || !setSelectedIds) {
      return;
    }

    let newSelectedIds = [...(selectedIds as Array<number>)];

    if (newSelectedIds.includes(id)) {
      newSelectedIds = newSelectedIds.filter((selectedId) => selectedId !== id);
    } else {
      newSelectedIds.push(id);
    }

    setSelectedIds(newSelectedIds);
  };

  const renderItem = (index: number, item: any) => {
    const params = {
      item,
      onRefresh,
      onSelect,
      isSelected: selectedIds?.includes(item?.id),
    };

    if (!animationOnItem) {
      return renderItemFromProps(params);
    }

    return (
      <Animatable.View animation="slideInUp" delay={page === 1 ? index * 100 : 0} duration={300}>
        {renderItemFromProps(params)}
      </Animatable.View>
    );
  };

  React.useEffect(() => {
    if (!data.length) {
      setIsFetching(true);
    }

    fetch();
  }, [fetch, data.length]);

  React.useEffect(() => {
    const unsubscribe = addListener('focus', () => {
      if (data.length) {
        setLoadedPages([]);
        setPage(1);
        setIsEmpty(false);
      }
    });

    return unsubscribe;
  }, [addListener, data.length]);

  React.useImperativeHandle(innerRef, () => ({
    onRefresh,
  }));

  const styles = getStyles(colors);

  if (isEmpty) {
    return (
      <EmptyList
        isFetching={isFetching}
        isFetchingMore={isFetchingMore}
        onRefresh={onRefresh}
        ListHeaderComponent={props.ListHeaderComponent}
        {...emptyListProps}
      />
    );
  }

  return (
    <FlatList
      style={styles.container}
      data={data}
      keyExtractor={(item) => item._id}
      initialNumToRender={10}
      ListFooterComponent={() => <FlatListFooter isFetchingMore={isFetchingMore} />}
      refreshControl={
        <RefreshControl refreshing={isFetching && !isFetchingMore} onRefresh={onRefresh} />
      }
      onEndReached={onLoadMore}
      onEndReachedThreshold={0.3}
      {...props}
      renderItem={({ item, index }) => renderItem(index, item)}
    />
  );
};

export default List;

const getStyles = (colors: IColors) =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.background,
    },
  });
