const colors = {
  white: "#FFFFFF",
  black: "#000000",

  background: "#F6F7F9",

  greyDarker: "#383D47",
  greyDark: "#717A8E",
  grey: "#B7BCC6",
  greyLight: "#DCDFE6",
  greyLighter: "#F6F7F9",

  brand: "#2BB9A6",
  info: "#24aeff",
  success: "#00d883",
  warning: "#ff8307",
  danger: "#ff0033",
};

export default colors;
