import { Platform } from "react-native";

let fonts = {
  primaryLight: "sans-serif-light",
  primaryRegular: "sans-serif",
  primarySemiBold: "sans-serif-condensed",
  primaryBold: "sans-serif-medium",
};

if (Platform.OS === "ios") {
  fonts = {
    primaryLight: "ArialHebrew-Light",
    primaryRegular: "ArialHebrew",
    primarySemiBold: "ArialHebrew-Bold",
    primaryBold: "Arial-BoldMT",
  };
}

export default fonts;
