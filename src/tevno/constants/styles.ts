export const removeMargin = {
  margin: 0,
  marginVertical: 0,
  marginHorizontal: 0,
  marginTop: 0,
  marginRight: 0,
  marginBottom: 0,
  marginLeft: 0,
};

export const removePadding = {
  padding: 0,
  paddingVertical: 0,
  paddingHorizontal: 0,
  paddingTop: 0,
  paddingRight: 0,
  paddingBottom: 0,
  paddingLeft: 0,
};
