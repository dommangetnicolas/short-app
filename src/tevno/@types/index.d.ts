declare interface String {
  truncate(length: number): string;
  capitalize(): string;
}
