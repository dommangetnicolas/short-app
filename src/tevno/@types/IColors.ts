export interface IColors {
  white: string;
  black: string;

  background: string;

  greyDarker: string;
  greyDark: string;
  grey: string;
  greyLight: string;
  greyLighter: string;

  brand: string;
  info: string;
  success: string;
  warning: string;
  danger: string;
}

export type Color =
  | "white"
  | "black"
  | "background"
  | "greyDarker"
  | "greyDark"
  | "grey"
  | "greyLight"
  | "greyLighter"
  | "brand"
  | "info"
  | "success"
  | "warning"
  | "danger";
