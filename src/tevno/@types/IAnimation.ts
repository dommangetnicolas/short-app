import * as Animatable from "react-native-animatable";

export interface IAnimation {
  animation: Animatable.Animation;
  duration?: number;
  delay?: number;
}
