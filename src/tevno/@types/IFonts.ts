export interface IFonts {
  primaryLight: string;
  primaryRegular: string;
  primarySemiBold: string;
  primaryBold: string;
}

export type Weight = "light" | "regular" | "semiBold" | "bold";
