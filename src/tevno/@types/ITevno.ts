import { IColors } from "./IColors";
import { IFonts } from "./IFonts";

export interface ITevnoProps {
  colors?: IColors;
  fonts?: IFonts;
}
