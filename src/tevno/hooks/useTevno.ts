import * as React from "react";
import { TevnoContext } from "../contexts/TevnoContext";

const useTevno = () => {
  const tevno = React.useContext(TevnoContext);

  return tevno;
};

export default useTevno;
