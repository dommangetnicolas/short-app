import {
  ActionSheetOptions,
  useActionSheet,
} from "@expo/react-native-action-sheet";

interface ActionType {
  title: string;
  callback?: () => void;
  cancel?: boolean;
  destructive?: boolean;
}

export interface ShowActionsFunction {
  (
    actions: Array<ActionType | false | undefined>,
    actionSheetOptions?:
      | Pick<
          ActionSheetOptions,
          | "title"
          | "textStyle"
          | "containerStyle"
          | "autoFocus"
          | "tintColor"
          | "icons"
          | "tintIcons"
          | "titleTextStyle"
          | "messageTextStyle"
          | "showSeparators"
          | "separatorStyle"
          | "useModal"
          | "destructiveColor"
          | "message"
          | "cancelButtonIndex"
          | "destructiveButtonIndex"
          | "anchor"
        >
      | undefined
  ): void;
}

const useActions = () => {
  const { showActionSheetWithOptions } = useActionSheet();

  const showActions: ShowActionsFunction = (
    actionsFromParams,
    actionSheetOptions
  ) => {
    const actions = actionsFromParams.filter(
      (action) => action && action?.title
    ) as ActionType[];

    const options = {
      options: actions.map((action) => action.title),
      cancelButtonIndex: actions.findIndex((item) => item.cancel),
      destructiveButtonIndex: actions.findIndex((item) => item.destructive),
      ...actionSheetOptions,
    };

    const callBack = (index: number) => actions[index]?.callback?.();

    showActionSheetWithOptions(options, callBack);
  };

  return { showActions };
};

export default useActions;
