import { Linking } from 'react-native';
import { InAppBrowser, InAppBrowserOptions } from 'react-native-inappbrowser-reborn';
import colors from '_constants/colors';

interface IShareFunction {
  (
    url: string,
    options?: {
      title?: string;
    },
  ): void;
}

export const openUrl = async (url: string) => {
  if (await Linking.canOpenURL(url)) {
    Linking.openURL(url);
  }
};

export const inAppBrowser = (url: string, options?: InAppBrowserOptions) => {
  return InAppBrowser.open(url, {
    dismissButtonStyle: 'done',
    preferredControlTintColor: colors.brand,
    animated: true,
    modalPresentationStyle: 'popover',
    modalTransitionStyle: 'coverVertical',
    modalEnabled: true,
    enableBarCollapsing: false,

    showTitle: true,
    secondaryToolbarColor: 'black',
    enableUrlBarHiding: true,
    enableDefaultShare: true,
    forceCloseOnRedirection: true,

    showInRecents: true,

    animations: {
      startEnter: 'slide_in_right',
      startExit: 'slide_out_left',
      endEnter: 'slide_in_left',
      endExit: 'slide_out_right',
    },

    ...options,
  });
};
