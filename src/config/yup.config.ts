import * as Yup from 'yup';

Yup.addMethod(Yup.mixed, 'domain', function (message) {
  return this.test({
    name: 'domain',
    message,
    test: (value) => {
      if (value === null || value === '' || value === undefined) {
        return false;
      }

      const domain = value.replace(/(^\w+:|^)\/\//, '');

      return /^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$/.test(
        domain,
      );
    },
  });
});

Yup.addMethod(Yup.mixed, 'equal', function (comparedValue, message) {
  return this.test({
    name: 'equal',
    message,
    test: (value) => {
      return value?.toLowerCase?.() === comparedValue?.toLowerCase?.();
    },
  });
});
