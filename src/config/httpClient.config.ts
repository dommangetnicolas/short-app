import axios, { AxiosError } from 'axios';
import { Alert } from 'react-native';
import env from '_environments/env.json';
import { store } from '_store/index';

const { getState } = store;

const httpClient = axios.create({
  baseURL: env.BASE_URL,
  headers: {
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache',
  },
  timeout: 10000,
});

httpClient.interceptors.request.use((config) => {
  const { token } = getState()?.Auth;

  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
    return config;
  }

  return config;
});

export const getHttpClientErr = (err: AxiosError) => {
  if (err?.response?.data?.message) {
    return err.response.data.message;
  }

  if (err?.response?.data) {
    return err.response.data;
  }

  if (err?.message) {
    switch (err?.message) {
      case 'Network Error':
        return 'Please try again later.';
    }

    return err?.message;
  }

  if (typeof err === 'string') {
    return err;
  }

  return 'Please try again later.';
};

let nbOfErrors = 0;
export const showHttpClientErr = (err: AxiosError) => {
  if (nbOfErrors > 0) return;

  nbOfErrors++;
  Alert.alert('Oops, an error has occured', getHttpClientErr(err));

  setTimeout(() => nbOfErrors--, 1800);
};

export default httpClient;
