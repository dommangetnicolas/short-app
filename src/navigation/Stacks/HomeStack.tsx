import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import HeaderBrand from '_components/Navigation/HeaderBrand';
import HeaderMenu from '_components/Navigation/HeaderMenu';
import HomeScreen from '_screens/HomeStack/HomeScreen';
import { screenOptionsDefault } from '../navigatorConfig';

const Stack = createStackNavigator();

const renderHomeStack = () => (
  <>
    <Stack.Screen
      name="HomeScreen"
      component={HomeScreen}
      options={{
        headerLeft: () => <HeaderMenu />,
        headerTitle: () => <HeaderBrand />,
      }}
    />
  </>
);

const HomeStack = () => (
  <Stack.Navigator
    screenOptions={() => ({
      ...screenOptionsDefault(),
    })}
  >
    {renderHomeStack()}
  </Stack.Navigator>
);

export default HomeStack;
