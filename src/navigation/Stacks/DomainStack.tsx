import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import HeaderBack from '_components/Navigation/HeaderBack';
import HeaderBrand from '_components/Navigation/HeaderBrand';
import HeaderButton from '_components/Navigation/HeaderButton';
import HeaderMenu from '_components/Navigation/HeaderMenu';
import DomainCreateScreen from '_screens/DomainStack/DomainCreateScreen';
import DomainDeletionScreen from '_screens/DomainStack/DomainDeletionScreen';
import DomainsScreen from '_screens/DomainStack/DomainsScreen';
import DomainVerificationScreen from '_screens/DomainStack/DomainVerificationScreen';
import { screenOptionsDefault } from '../navigatorConfig';

const Stack = createStackNavigator();

const renderDomainStack = () => (
  <>
    <Stack.Screen
      name="DomainsScreen"
      component={DomainsScreen}
      options={{
        headerLeft: () => <HeaderMenu />,
        headerTitle: () => <HeaderBrand />,
        headerRight: () => <HeaderButton icon="add" to="DomainCreateScreen" />,
      }}
    />

    <Stack.Screen
      name="DomainCreateScreen"
      component={DomainCreateScreen}
      options={{
        headerLeft: ({ canGoBack, onPress }) => (
          <HeaderBack canGoBack={canGoBack} onPress={onPress} />
        ),
        headerTitle: () => <HeaderBrand />,
      }}
    />

    <Stack.Screen
      name="DomainVerificationScreen"
      component={DomainVerificationScreen}
      options={{
        headerLeft: ({ canGoBack, onPress }) => (
          <HeaderBack canGoBack={canGoBack} onPress={onPress} />
        ),
        headerTitle: () => <HeaderBrand />,
      }}
    />

    <Stack.Screen
      name="DomainDeletionScreen"
      component={DomainDeletionScreen}
      options={{
        headerLeft: ({ canGoBack, onPress }) => (
          <HeaderBack canGoBack={canGoBack} onPress={onPress} />
        ),
        headerTitle: () => <HeaderBrand />,
      }}
    />
  </>
);

const DomainStack = () => (
  <Stack.Navigator
    screenOptions={() => ({
      ...screenOptionsDefault(),
    })}
  >
    {renderDomainStack()}
  </Stack.Navigator>
);

export default DomainStack;
