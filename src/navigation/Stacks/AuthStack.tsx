import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import AuthScreen from '_screens/AuthStack/AuthScreen';
import SigninScreen from '_screens/AuthStack/SigninScreen';
import SignupScreen from '_screens/AuthStack/SignupScreen';
import { screenOptionsDefault } from '../navigatorConfig';

const Stack = createStackNavigator();

export const renderAuthStack = () => (
  <>
    <Stack.Screen name="AuthScreen" component={AuthScreen} options={{ headerShown: false }} />
    <Stack.Screen name="SigninScreen" component={SigninScreen} options={{ headerShown: false }} />
    <Stack.Screen name="SignupScreen" component={SignupScreen} />
  </>
);

const AuthStack = () => (
  <Stack.Navigator
    screenOptions={() => ({
      headerTitle: '',
      ...screenOptionsDefault(),
    })}
  >
    {renderAuthStack()}
  </Stack.Navigator>
);

export default AuthStack;
