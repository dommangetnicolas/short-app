import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import HeaderBack from '_components/Navigation/HeaderBack';
import HeaderBrand from '_components/Navigation/HeaderBrand';
import HeaderMenu from '_components/Navigation/HeaderMenu';
import LinkEditScreen from '_screens/LinkStack/LinkEditScreen';
import LinkScreen from '_screens/LinkStack/LinkScreen';
import LinksScreen from '_screens/LinkStack/LinksScreen';
import { screenOptionsDefault } from '../navigatorConfig';

const Stack = createStackNavigator();

const renderLinkStack = () => (
  <>
    <Stack.Screen
      name="LinksScreen"
      component={LinksScreen}
      options={{
        headerLeft: () => <HeaderMenu />,
        headerTitle: () => <HeaderBrand />,
      }}
    />

    <Stack.Screen
      name="LinkScreen"
      component={LinkScreen}
      options={{
        headerLeft: ({ canGoBack, onPress }) => (
          <HeaderBack canGoBack={canGoBack} onPress={onPress} />
        ),
        headerTitle: () => <HeaderBrand />,
      }}
    />

    <Stack.Screen
      name="LinkEditScreen"
      component={LinkEditScreen}
      options={{
        headerLeft: ({ canGoBack, onPress }) => (
          <HeaderBack canGoBack={canGoBack} onPress={onPress} />
        ),
        headerTitle: () => <HeaderBrand />,
      }}
    />
  </>
);

const LinkStack = () => (
  <Stack.Navigator
    screenOptions={() => ({
      ...screenOptionsDefault(),
    })}
  >
    {renderLinkStack()}
  </Stack.Navigator>
);

export default LinkStack;
