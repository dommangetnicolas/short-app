import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import HeaderBack from '_components/Navigation/HeaderBack';
import HeaderBrand from '_components/Navigation/HeaderBrand';
import HeaderMenu from '_components/Navigation/HeaderMenu';
import UserDeletionScreen from '_screens/UserStack/UserDeletionScreen';
import UserEditScreen from '_screens/UserStack/UserEditScreen';
import UserScreen from '_screens/UserStack/UserScreen';
import UserScreenHeader from '_screens/UserStack/UserScreenHeader';
import { screenOptionsDefault } from '../navigatorConfig';

const Stack = createStackNavigator();

const renderUserStack = () => (
  <>
    <Stack.Screen
      name="UserScreen"
      component={UserScreen}
      options={{
        headerLeft: () => <HeaderMenu />,
        headerTitle: () => <HeaderBrand />,
        headerRight: () => <UserScreenHeader />,
      }}
    />

    <Stack.Screen
      name="UserEditScreen"
      component={UserEditScreen}
      options={{
        headerLeft: ({ canGoBack, onPress }) => (
          <HeaderBack canGoBack={canGoBack} onPress={onPress} />
        ),
        headerTitle: () => <HeaderBrand />,
      }}
    />

    <Stack.Screen
      name="UserDeletionScreen"
      component={UserDeletionScreen}
      options={{
        headerLeft: ({ canGoBack, onPress }) => (
          <HeaderBack canGoBack={canGoBack} onPress={onPress} />
        ),
        headerTitle: () => <HeaderBrand />,
      }}
    />
  </>
);

const UserStack = () => (
  <Stack.Navigator
    screenOptions={() => ({
      ...screenOptionsDefault(),
    })}
  >
    {renderUserStack()}
  </Stack.Navigator>
);

export default UserStack;
