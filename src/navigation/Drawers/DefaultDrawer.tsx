import { createDrawerNavigator } from '@react-navigation/drawer';
import React, { FunctionComponent } from 'react';
import { Dimensions, StyleSheet } from 'react-native';
import DrawerComponent from '_components/Navigation/Drawer';
import DomainStack from '_navigation/Stacks/DomainStack';
import HomeStack from '_navigation/Stacks/HomeStack';
import LinkStack from '_navigation/Stacks/LinkStack';
import UserStack from '_navigation/Stacks/UserStack';

const Drawer = createDrawerNavigator();

const DefaultDrawer: FunctionComponent = () => {
  return (
    <Drawer.Navigator
      initialRouteName="Home"
      drawerContent={(props) => <DrawerComponent {...props} />}
      drawerStyle={styles.container}
    >
      <Drawer.Screen name="HomeStack" component={HomeStack} />
      <Drawer.Screen name="LinkStack" component={LinkStack} />
      <Drawer.Screen name="DomainStack" component={DomainStack} />
      <Drawer.Screen name="UserStack" component={UserStack} />
    </Drawer.Navigator>
  );
};

export default DefaultDrawer;

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width * 0.6,
  },
});
