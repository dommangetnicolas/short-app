import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '_store/index.d';
import DefaultDrawer from './Drawers/DefaultDrawer';
import AuthStack from './Stacks/AuthStack';

const Stack = createStackNavigator();

const navigationRef = React.createRef<any>();

export const navigate = (name: any, params: any) => navigationRef.current?.navigate(name, params);

const Navigator = () => {
  const { token } = useSelector((state: RootState) => state.Auth);

  const isAuthenticated = !!token;

  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator screenOptions={{ headerShown: false }} mode="modal">
        {isAuthenticated && (
          <Stack.Screen
            name="DefaultDrawer"
            component={DefaultDrawer}
            options={{ animationEnabled: false }}
          />
        )}

        {!isAuthenticated && (
          <Stack.Screen
            name="AuthStack"
            component={AuthStack}
            options={{ animationEnabled: false }}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;
