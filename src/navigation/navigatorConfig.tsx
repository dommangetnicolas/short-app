import { StackNavigationOptions } from '@react-navigation/stack';
import React from 'react';
import HeaderBack from '_components/Navigation/HeaderBack';
import colors from '_constants/colors';
import fonts from '_constants/fonts';

const screenOptionsDefault = (): StackNavigationOptions => {
  return {
    headerStyle: {
      backgroundColor: colors.background,
      shadowOffset: { height: 0, width: 0 },
      elevation: 0,
      shadowOpacity: 0,
      borderBottomWidth: 0,
    },
    headerTitleStyle: {
      fontSize: 18,
      fontFamily: fonts.primarySemiBold,
    },
    headerLeft: ({ canGoBack, onPress }) => <HeaderBack canGoBack={canGoBack} onPress={onPress} />,
  };
};

export { screenOptionsDefault };
