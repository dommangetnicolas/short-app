import { ActionSheetProvider } from '@expo/react-native-action-sheet';
import React, { FunctionComponent } from 'react';
import { StatusBar } from 'react-native';
import 'react-native-gesture-handler';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import '_config/dayjs.config';
import '_config/yup.config';
import Navigator from '_navigation/index';
import colors from './constants/colors';
import fonts from './constants/fonts';
import { persistor, store } from './store';
import { TevnoProvider } from './tevno/contexts/TevnoContext';

const App: FunctionComponent = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ActionSheetProvider>
          <TevnoProvider colors={colors} fonts={fonts}>
            <StatusBar barStyle="dark-content" backgroundColor={colors.background} />
            <Navigator />
          </TevnoProvider>
        </ActionSheetProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
