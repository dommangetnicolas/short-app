import { Reducer } from 'redux';
import { AuthActionsPayload, AuthState } from './auth/types';
import { UserState } from './user/types';

export type RootState = {
  Auth: AuthState;
  User: UserState;
};

export type RootActions = AuthActionsPayload;

export type RootReducer = Reducer<RootState, RootActions>;
