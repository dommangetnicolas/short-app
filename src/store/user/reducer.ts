import { UserActions, UserActionsPayload, UserState } from './types.d';

const initialState: UserState = {
  user: null,
};

const reducer = (state = initialState, action: UserActionsPayload) => {
  switch (action.type) {
    case UserActions.SET_USER:
      return { ...state, user: action.user };

    default:
      return state;
  }
};

export default reducer;
