import { Dispatch } from 'redux';
import ApiUser from '_api/user.api';
import { showHttpClientErr } from '_config/httpClient.config';
import { SetUser, UserActions } from './types.d';

export const fetchUser = () => async (dispatch: Dispatch) => {
  try {
    const { data } = await ApiUser.get();

    dispatch(setUser(data));
  } catch (err) {
    showHttpClientErr(err);
  }
};

export const setUser: SetUser = (user) => ({
  type: UserActions.SET_USER,
  user,
});
