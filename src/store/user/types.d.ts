import { IUserModel } from '_types/models/UserModel/IUserModel';

/**
 * Action Types
 */
export enum UserActions {
  SET_USER = 'SET_USER',
}

/**
 * States
 */
export interface UserState {
  user: IUserModel | null;
}

/**
 * Actions Payloads
 */
export interface ISetUserPayload {
  type: UserActions;
  user: IUserModel | null;
}

export type UserActionsPayload = ISetUserPayload;

/**
 * Actions
 */
export type SetUser = {
  (user: IUserModel): ISetUserPayload;
};
