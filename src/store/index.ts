import AsyncStorage from '@react-native-async-storage/async-storage';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk';
import Auth from './auth/reducer';
import User from './user/reducer';
import { AuthActions } from './auth/types.d';
import { RootReducer } from './index.d';

const appReducer = combineReducers({
  Auth,
  User,
});

const rootReducer: RootReducer = (state, action) => {
  if (action.type === AuthActions.SIGNOUT) {
    state = undefined;
  }

  return appReducer(state, action);
};

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistedReducer, applyMiddleware(thunk));
export const persistor = persistStore(store as any);
