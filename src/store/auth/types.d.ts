/**
 * Action Types
 */

export enum AuthActions {
  SET_TOKEN = 'SET_TOKEN',
  SIGNOUT = 'SIGNOUT',
}

/**
 * States
 */
export interface AuthState {
  token: string | null;
}

/**
 * Actions Payloads
 */
export interface IAuthSetTokenPayload {
  type: AuthActions;
  token: string | null;
}

export interface IAuthSignoutPayload {
  type: AuthActions;
}

export type AuthActionsPayload = IAuthSetTokenPayload & IAuthSignoutPayload;

/**
 * Actions
 */
export type AuthSetToken = {
  (token: string): IAuthSetTokenPayload;
};

export type AuthSignout = {
  (): IAuthSignoutPayload;
};
