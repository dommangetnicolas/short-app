import { AuthActionsPayload } from './types.d';
import { AuthActions, AuthState } from './types.d';

const initialState: AuthState = {
  token: null,
};

const reducer = (state = initialState, action: AuthActionsPayload) => {
  switch (action.type) {
    case AuthActions.SET_TOKEN:
      return { ...state, token: action.token };

    default:
      return state;
  }
};

export default reducer;
