import { AuthActions, AuthSetToken, AuthSignout } from './types.d';

export const setToken: AuthSetToken = (token) => ({
  type: AuthActions.SET_TOKEN,
  token,
});

export const signout: AuthSignout = () => ({
  type: AuthActions.SIGNOUT,
});
