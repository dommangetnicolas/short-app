import { DrawerContentComponentProps, DrawerContentOptions } from '@react-navigation/drawer';
import { CommonActions } from '@react-navigation/routers';
import React, { FunctionComponent } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import colors from '_constants/colors';
import { Text } from '_tevno/index';

interface IProps extends DrawerContentComponentProps<DrawerContentOptions> {
  icon: JSX.Element;
  title: string;
  to?: string;
  onPress?(): void;
}

const DrawerItem: FunctionComponent<IProps> = (props) => {
  const { icon, title, to, onPress: onPressFromProps } = props;

  const { dispatch } = props.navigation;

  const onPress = () => {
    if (to) {
      dispatch({
        ...CommonActions.reset({
          index: 0,
          routes: [{ name: to }],
        }),
      });
      return;
    }

    onPressFromProps?.();
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.contentContainer}
        disabled={!to && !onPressFromProps}
        onPress={onPress}
      >
        <View style={styles.iconContainer}>
          {React.cloneElement(icon, { color: colors.brand, size: 20 })}
        </View>

        <Text style={styles.text}>{title}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default DrawerItem;

const styles = StyleSheet.create({
  container: {
    marginBottom: 15,
  },
  contentContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  iconContainer: {
    minWidth: 30,
    alignItems: 'center',
  },
  text: {
    fontSize: 18,
    marginLeft: 10,
    color: colors.brand,
    flex: 1,
  },
});
