import { StackHeaderLeftButtonProps } from '@react-navigation/stack';
import React, { FunctionComponent } from 'react';
import HeaderButton from './HeaderButton';

const HeaderBack: FunctionComponent<StackHeaderLeftButtonProps> = ({ canGoBack, onPress }) => {
  if (!canGoBack || !onPress) {
    return null;
  }

  return <HeaderButton icon="arrow-back" onPress={onPress} />;
};

export default HeaderBack;
