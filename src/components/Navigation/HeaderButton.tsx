import { useNavigation } from '@react-navigation/core';
import React, { FunctionComponent } from 'react';
import { StyleSheet, TouchableOpacity, View, ViewStyle } from 'react-native';
import colors from '_constants/colors';
import { IconNames } from '_tevno/@types/Icon';
import { Icon, Spinner } from '_tevno/index';

export interface IHeaderButtonProps {
  icon: IconNames;
  containerStyle?: ViewStyle;
  isPending?: boolean;
  to?: string;
  onPress?: () => void;
}

const HeaderButton: FunctionComponent<IHeaderButtonProps> = (props) => {
  const { icon, containerStyle, isPending, to, onPress: onPressFromProps } = props;

  const { navigate } = useNavigation();

  const onPress = () => {
    if (to) {
      navigate(to);
      return;
    }

    onPressFromProps?.();
  };

  if (isPending) {
    return (
      <View style={[styles.container, containerStyle]}>
        <Spinner size={33} />
      </View>
    );
  }

  return (
    <TouchableOpacity onPress={onPress} style={[styles.container, containerStyle]}>
      <Icon name={icon} size={32} color={colors.brand} />
    </TouchableOpacity>
  );
};

export default HeaderButton;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
});
