import { DrawerContentComponentProps, DrawerContentOptions } from '@react-navigation/drawer';
import { DrawerActions } from '@react-navigation/native';
import React, { FunctionComponent, useEffect } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { signout } from '_store/auth/actions';
import { fetchUser } from '_store/user/actions';
import { Icon, useActions } from '_tevno/index';
import DrawerItem from './DrawerItem';
import HeaderButton from './HeaderButton';

const Drawer: FunctionComponent<DrawerContentComponentProps<DrawerContentOptions>> = (props) => {
  const { dispatch } = props.navigation;
  const dispatchStore = useDispatch();
  const { showActions } = useActions();

  useEffect(() => {
    dispatchStore(fetchUser());
  }, [dispatchStore]);

  const onSignout = () => {
    showActions([
      {
        title: 'Signout',
        destructive: true,
        callback: () => dispatchStore(signout()),
      },
      {
        title: 'Cancel',
        cancel: true,
      },
    ]);
  };

  const drawerItems = [
    {
      icon: <Icon name="home-outline" size={28} />,
      title: 'Home',
      to: 'HomeStack',
    },
    {
      icon: <Icon name="link-outline" size={28} />,
      title: 'Links',
      to: 'LinkStack',
    },
    {
      icon: <Icon name="planet-outline" size={28} />,
      title: 'Domains',
      to: 'DomainStack',
    },
    {
      icon: <Icon name="person-outline" size={28} />,
      title: 'Account',
      to: 'UserStack',
    },
  ];

  return (
    <View style={styles.container}>
      <SafeAreaView />

      <HeaderButton
        icon="close"
        onPress={() => dispatch(DrawerActions.closeDrawer())}
        containerStyle={styles.crossTouchable}
      />

      <ScrollView contentContainerStyle={styles.contentContainer}>
        {drawerItems.map((item, key) => (
          <DrawerItem key={key} {...item} {...props} />
        ))}
      </ScrollView>

      <View style={styles.bottomContentContainer}>
        <DrawerItem
          icon={<Icon name="log-out-outline" size={28} />}
          title="Signout"
          onPress={onSignout}
          {...props}
        />
      </View>

      <SafeAreaView />
    </View>
  );
};

export default Drawer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    flexGrow: 1,
    padding: 20,
  },
  bottomContentContainer: {
    padding: 20,
  },
  crossTouchable: {
    marginTop: -10,
    padding: 20,
  },
});
