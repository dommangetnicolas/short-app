import { CommonActions, useNavigation } from '@react-navigation/core';
import React, { FunctionComponent } from 'react';
import { TouchableOpacity } from 'react-native';
import BrandSvg from '_assets/images/BrandSvg';

const HeaderBrand: FunctionComponent = () => {
  const { dispatch } = useNavigation();

  const onPress = () => {
    dispatch({
      ...CommonActions.reset({
        index: 0,
        routes: [{ name: 'HomeStack' }],
      }),
    });
  };

  return (
    <TouchableOpacity onPress={onPress}>
      <BrandSvg width={227} height={30} />
    </TouchableOpacity>
  );
};

export default HeaderBrand;
