import { DrawerActions, useNavigation } from '@react-navigation/core';
import React, { FunctionComponent } from 'react';
import HeaderButton from './HeaderButton';

const HeaderMenu: FunctionComponent = () => {
  const { dispatch } = useNavigation();

  return <HeaderButton icon="menu" onPress={() => dispatch(DrawerActions.openDrawer())} />;
};

export default HeaderMenu;
