import { useNavigation } from '@react-navigation/core';
import dayjs from 'dayjs';
import React, { FunctionComponent } from 'react';
import { StyleSheet, View } from 'react-native';
import colors from '_constants/colors';
import { SwipeableActionPayload } from '_tevno/components/SwipeableSticker';
import { Button, Card, Chips, SwipeableSticker, Text } from '_tevno/index';
import { IDomainModel } from '_types/models/DomainModel/IDomainModel';

interface IProps {
  item: IDomainModel;
}

const DomainOverview: FunctionComponent<IProps> = (props) => {
  const { domain, createdAt, verifiedAt } = { ...props.item };

  const { navigate } = useNavigation();

  const onPress = () => {
    if (!verifiedAt) {
      navigate('DomainVerificationScreen', { domain: props.item });
    }
  };

  const onDelete = () => {
    navigate('DomainDeletionScreen', { domain: props.item });
  };

  const rightActions: SwipeableActionPayload[] = [
    {
      icon: 'trash-outline',
      iconProps: { color: colors.danger },
      onPress: onDelete,
    },
  ];

  return (
    <SwipeableSticker
      containerStyle={styles.swipeable}
      onPressDisabled={!!verifiedAt}
      onPress={onPress}
      rightActions={rightActions}
    >
      <Card bodyStyle={styles.container}>
        <View>
          <Text style={styles.date}>{`created on ${dayjs(createdAt).format('MMM DD')}`}</Text>
          <Text>{domain.capitalize()}</Text>

          {!verifiedAt && (
            <Chips style={styles.verified} color="warning" outline>
              Waiting verification
            </Chips>
          )}

          {!!verifiedAt && (
            <Chips style={styles.verified} color="brand" outline>
              Verified
            </Chips>
          )}
        </View>

        {!verifiedAt && <Button nude icon="arrow-forward" color="brand" onPress={onPress} />}
      </Card>
    </SwipeableSticker>
  );
};

export default DomainOverview;

const styles = StyleSheet.create({
  swipeable: {
    paddingHorizontal: 20,
    paddingVertical: 5,
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  date: {
    fontSize: 12,
    color: colors.greyDark,
    marginBottom: 5,
  },
  verified: {
    marginTop: 10,
  },
});
