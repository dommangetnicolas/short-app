import { useNavigation } from '@react-navigation/core';
import dayjs from 'dayjs';
import React, { FunctionComponent } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import colors from '_constants/colors';
import fonts from '_constants/fonts';
import { SwipeableActionPayload } from '_tevno/components/SwipeableSticker';
import { Button, Card, SwipeableSticker, Text } from '_tevno/index';
import { ILinkModel } from '_types/models/LinkModel/ILinkModel';
import { inAppBrowser } from '_utils/share.utils';

interface IProps {
  item: ILinkModel;
}

const LinkOverview: FunctionComponent<IProps> = (props) => {
  const { _id, shortLink, target, name, domain, createdAt } = { ...props.item };

  const { navigate } = useNavigation();

  const onPress = () => navigate('LinkScreen', { _id });

  const onDelete = () => {};

  const rightActions: SwipeableActionPayload[] = [
    {
      icon: 'trash-outline',
      iconProps: { color: colors.danger },
      onPress: onDelete,
    },
  ];

  return (
    <SwipeableSticker
      containerStyle={styles.swipeable}
      onPress={onPress}
      rightActions={rightActions}
    >
      <Card bodyStyle={styles.container}>
        <View>
          <Text style={styles.date}>{dayjs(createdAt).format('MMM DD')}</Text>
          <Text>{name}</Text>

          <TouchableOpacity style={styles.link} onPress={() => inAppBrowser(target)}>
            {domain && <Text style={styles.linkText}>{domain.domain}</Text>}
            <Text style={[styles.linkText, styles.path]}>{`/${shortLink}`}</Text>
          </TouchableOpacity>
        </View>

        <Button nude icon="arrow-forward" color="brand" onPress={onPress} />
      </Card>
    </SwipeableSticker>
  );
};

export default LinkOverview;

const styles = StyleSheet.create({
  swipeable: {
    paddingVertical: 5,
    paddingHorizontal: 20,
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  date: {
    fontSize: 12,
    color: colors.greyDark,
    marginBottom: 5,
  },
  link: {
    alignSelf: 'flex-start',
    paddingTop: 10,
    flexDirection: 'row',
  },
  linkText: {
    fontSize: 14,
    color: colors.brand,
  },
  path: {
    fontFamily: fonts.primarySemiBold,
  },
});
