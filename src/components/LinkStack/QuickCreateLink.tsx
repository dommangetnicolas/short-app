import { Formik, FormikHelpers } from 'formik';
import React, { FunctionComponent } from 'react';
import { Alert, StyleSheet, View } from 'react-native';
import * as Yup from 'yup';
import ApiLink from '_api/link.api';
import { showHttpClientErr } from '_config/httpClient.config';
import defaultStyles from '_constants/styles';
import { ICardProps } from '_tevno/components/Card';
import { Button, Card, Text, TextInput } from '_tevno/index';

interface FormValues {
  target: string;
}

const validation = Yup.object({
  target: Yup.string().required(),
});

const QuickCreateLink: FunctionComponent<ICardProps> = (props) => {
  const onSubmit = async (values: FormValues, { resetForm }: FormikHelpers<FormValues>) => {
    try {
      const { data } = await ApiLink.create(values);

      resetForm({});
      Alert.alert('Success !', `Your link has been created as ${data.shortLink}`);
    } catch (err) {
      showHttpClientErr(err);
    }
  };

  return (
    <Formik onSubmit={onSubmit} initialValues={{ target: '' }} validationSchema={validation}>
      {({ handleBlur, handleChange, handleSubmit, isSubmitting, errors, touched, values }) => (
        <Card {...props}>
          <Text style={[defaultStyles.title, styles.title]}>Quick create</Text>

          <View style={styles.linkContainer}>
            <TextInput
              style={styles.linkInput}
              placeholder="Target link"
              value={values.target}
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={handleChange('target')}
              onBlur={handleBlur('target')}
              isInvalid={!!(touched.target && errors.target)}
            />
            <Button
              color="brand"
              isPending={isSubmitting}
              onPress={() => handleSubmit()}
              round
              size="md"
              icon="add"
            />
          </View>
        </Card>
      )}
    </Formik>
  );
};

export default QuickCreateLink;

const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    marginBottom: 10,
  },
  linkContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  linkInput: {
    flex: 1,
    marginRight: 15,
  },
});
