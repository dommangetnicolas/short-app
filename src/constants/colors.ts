const colors = {
  white: '#FFFFFF',
  black: '#000000',

  background: '#F1F2F6',

  greyDarker: '#333333',
  greyDark: '#777777',
  grey: '#CCCCCC',
  greyLight: '#DDDDDD',
  greyLighter: '#EEEEEE',

  brand: '#2F80ED',
  info: '#56CCF2',
  success: '#3AE374',
  warning: '#FF9F1A',
  danger: '#FF3838',
};

export default colors;
