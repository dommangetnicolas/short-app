import colors from './colors';
import fonts from './fonts';

const scrollWiew = {
  flexGrow: 1,
  paddingHorizontal: 20,
  paddingBottom: 20,
};

const title = {
  color: colors.greyDarker,
  fontFamily: fonts.primaryBold,
  fontSize: 30,
  marginBottom: 15,
};

const subTitle = {
  color: colors.brand,
  fontFamily: fonts.primaryBold,
  fontSize: 23,
  marginTop: -10,
  marginBottom: 15,
};

const key = {
  fontSize: 14,
  color: colors.greyDark,
};

const value = {
  fontFamily: fonts.primarySemiBold,
};

const defaultStyles = {
  scrollWiew,
  title,
  subTitle,
  key,
  value,
};

export default defaultStyles;
