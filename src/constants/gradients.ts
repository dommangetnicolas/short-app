import colors from './colors';

const gradients = {
  primary: [colors.brand, colors.info],
};

export default gradients;
