const fonts = {
  primaryLight: 'Raleway-Light',
  primaryRegular: 'Raleway-Medium',
  primarySemiBold: 'Raleway-SemiBold',
  primaryBold: 'Raleway-Bold',
};

export default fonts;
