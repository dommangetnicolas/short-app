#!/bin/node
const fs = require('fs');

const environment = process.argv[2] || 'development';

fs.copyFile(`${__dirname}/env/${environment}.env.json`, `${__dirname}/env.json`, (err) => {
  if (err) {
    throw err;
  }

  console.log(`${__dirname}/${environment}.env.json was copied to ${__dirname}/env.json`);
});
