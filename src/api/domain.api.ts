import httpClient from '_config/httpClient.config';
import { IListFilters } from '_types/api/IListFilters';
import { IListResponse } from '_types/api/IListResponse';
import { ICreateDomain } from '_types/models/DomainModel/ICreateDomain';
import { IDeleteDomain } from '_types/models/DomainModel/IDeleteDomain';
import { IDomainModel } from '_types/models/DomainModel/IDomainModel';

class ApiDomain {
  static list(data: IListFilters) {
    return httpClient.post<IListResponse<IDomainModel>>('/v1/domain/list', data);
  }

  static create(data: ICreateDomain) {
    return httpClient.post<IDomainModel>(`/v1/domain/`, data);
  }

  static delete(_id: string, data: IDeleteDomain) {
    return httpClient.delete(`/v1/domain/${_id}`, { data });
  }
}

export default ApiDomain;
