import httpClient from '_config/httpClient.config';
import { IDomainVerificationModel } from '_types/models/DomainVerificationModel/IDomainVerificationModel';

class ApiDomainVerification {
  static get(domainId: string) {
    return httpClient.get<IDomainVerificationModel>(`/v1/domain/${domainId}/verification`);
  }

  static verify(domainId: string) {
    return httpClient.post<IDomainVerificationModel>(`/v1/domain/${domainId}/verification`);
  }
}

export default ApiDomainVerification;
