import httpClient from '_config/httpClient.config';
import { IListResponse } from '_types/api/IListResponse';
import { ICreateLink } from '_types/models/LinkModel/ICreateLink';
import { IGetLink } from '_types/models/LinkModel/IGetLink';
import { ILinkModel } from '_types/models/LinkModel/ILinkModel';
import { IListLink } from '_types/models/LinkModel/IListLink';
import { IUpdateLink } from '_types/models/LinkModel/IUpdateLink';

class ApiLink {
  static create(data: ICreateLink) {
    return httpClient.post('/v1/link', data);
  }

  static list(data: IListLink) {
    return httpClient.post<IListResponse<ILinkModel>>('/v1/link/list', data);
  }

  static get(_id: string, query?: IGetLink) {
    return httpClient.get<ILinkModel>(`/v1/link/${_id}`, { params: query });
  }

  static update(_id: string, data: IUpdateLink) {
    return httpClient.put(`/v1/link/${_id}`, data);
  }

  static delete(_id: string) {
    return httpClient.delete(`/v1/link/${_id}`);
  }
}

export default ApiLink;
