import httpClient from '_config/httpClient.config';
import { ISignup } from '_types/models/AuthModel/ISignup';

class ApiAuth {
  static signIn(email: string, password: string) {
    return httpClient.post('/v1/auth/signin', {
      email,
      password,
    });
  }

  static signUp(data: ISignup) {
    return httpClient.post('/v1/auth/signup', data);
  }
}

export default ApiAuth;
