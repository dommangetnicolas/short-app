import httpClient from '_config/httpClient.config';
import { IDeleteUser } from '_types/models/UserModel/IDeleteUser';
import { IUpdateUser } from '_types/models/UserModel/IUpdateUser';
import { IUserModel } from '_types/models/UserModel/IUserModel';

class ApiUser {
  static get() {
    return httpClient.get<IUserModel>('/v1/user');
  }

  static update(data: IUpdateUser) {
    return httpClient.put(`/v1/user`, data);
  }

  static delete(data: IDeleteUser) {
    return httpClient.delete(`/v1/user`, { data });
  }
}

export default ApiUser;
