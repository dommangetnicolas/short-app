export interface IListFilters {
  sort?: Object | string;
  page?: {
    index: number;
    size: number;
  };
}
