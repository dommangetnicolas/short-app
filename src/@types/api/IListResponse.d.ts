export interface IListResponse<Item> {
  docs: Item[];
  totalDocs: number;
  limit: number;
  totalPages: number;
  page: number;
}
