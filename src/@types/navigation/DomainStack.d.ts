import { IDomainModel } from '_types/models/DomainModel/IDomainModel';

export type DomainStackParams = {
  DomainVerificationScreen: {
    domain?: IDomainModel;
  };
  DomainDeletionScreen: {
    domain?: IDomainModel;
  };
};
