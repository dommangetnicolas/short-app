import { ILinkModel } from '_types/models/LinkModel/ILinkModel';

export type LinkStackParams = {
  LinkScreen: {
    _id?: string;
  };
  LinkEditScreen: {
    data?: ILinkModel;
  };
};
