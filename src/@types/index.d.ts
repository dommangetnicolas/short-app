/* eslint-disable @typescript-eslint/no-unused-vars */
import * as yup from 'yup';

declare module 'yup' {
  interface StringSchema {
    domain(): StringSchema;
    equal(comparedValue: string): StringSchema;
  }
}
