export interface IDomainVerificationModel {
  userId: string;

  domainId: string;

  uuid: string;

  createdAt: Date;

  updatedAt: Date;
}
