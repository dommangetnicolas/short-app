export interface IDomainModel {
  _id: string;

  userId: string;

  domain: string;

  verifiedAt: Date | null;

  createdAt: Date;

  updatedAt: Date;
}
