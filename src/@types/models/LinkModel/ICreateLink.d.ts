export interface ICreateLink {
  domainId?: string;

  name?: string;

  shortLink?: string;

  target: string;
}
