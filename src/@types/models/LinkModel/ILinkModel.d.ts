import { IDomainModel } from '../DomainModel/IDomainModel';

export interface ILinkModel {
  _id: string;

  userId: string;

  domainId: string;

  name: string;

  shortLink: string;

  target: string;

  createdAt: Date;

  updatedAt: Date;

  domain?: IDomainModel;
}
