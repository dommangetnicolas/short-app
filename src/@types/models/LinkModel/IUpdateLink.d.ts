export interface IUpdateLink {
  domainId?: string;

  name?: string;

  target?: string;

  shortLink?: string;
}
