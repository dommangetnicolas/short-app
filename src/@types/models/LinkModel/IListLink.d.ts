import { IListFilters } from '_types/api/IListFilters';

export interface IListLink extends IListFilters {
  populate?: {
    domain?: boolean;
  };
}
