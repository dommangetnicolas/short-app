export interface IUpdateUser {
  firstName?: string;
  lastName?: string;
}
