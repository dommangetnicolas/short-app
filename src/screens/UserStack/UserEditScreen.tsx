import { Formik } from 'formik';
import React, { FunctionComponent, useRef } from 'react';
import { Alert, ScrollView, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import * as Yup from 'yup';
import ApiUser from '_api/user.api';
import { showHttpClientErr } from '_config/httpClient.config';
import defaultStyles from '_constants/styles';
import { RootState } from '_store/index.d';
import { Button, Card, Screen, Text, TextInput } from '_tevno/index';

interface FormValues {
  firstName: string;
  lastName: string;
}

const validation = Yup.object({
  firstName: Yup.string().min(1),
  lastName: Yup.string().min(1),
});

const UserEditScreen: FunctionComponent = () => {
  const { user } = useSelector((state: RootState) => state.User);

  const lastNameRef = useRef<any>();

  if (!user) return null;

  const onSubmit = async (values: FormValues) => {
    try {
      await ApiUser.update(values);
      Alert.alert('Account updated', `Your information has been updated.`);
    } catch (err) {
      showHttpClientErr(err);
    }
  };

  return (
    <Formik onSubmit={onSubmit} initialValues={{ ...user }} validationSchema={validation}>
      {({ handleChange, handleBlur, handleSubmit, isSubmitting, values, errors, touched }) => (
        <Screen>
          <ScrollView contentContainerStyle={styles.scrollView}>
            <Text style={defaultStyles.title}>My account</Text>
            <Text style={defaultStyles.subTitle}>Update information</Text>

            <Card>
              <TextInput
                style={styles.input}
                placeholder="First name"
                onChangeText={handleChange('firstName')}
                onBlur={handleBlur('firstName')}
                value={values.firstName}
                autoCorrect={false}
                returnKeyType="next"
                blurOnSubmit={false}
                onSubmitEditing={() => lastNameRef.current.focus()}
                isInvalid={!!(touched.firstName && errors.firstName)}
              />

              <TextInput
                ref={lastNameRef}
                style={styles.input}
                placeholder="Last name"
                onChangeText={handleChange('lastName')}
                onBlur={handleBlur('lastName')}
                value={values.lastName}
                autoCorrect={false}
                returnKeyType="next"
                blurOnSubmit={false}
                isInvalid={!!(touched.lastName && errors.lastName)}
              />

              <Button color="brand" block isPending={isSubmitting} onPress={() => handleSubmit()}>
                Update information
              </Button>
            </Card>
          </ScrollView>
        </Screen>
      )}
    </Formik>
  );
};

export default UserEditScreen;

const styles = StyleSheet.create({
  scrollView: {
    paddingHorizontal: 20,
  },
  input: {
    marginBottom: 15,
  },
});
