import { Formik } from 'formik';
import React, { FunctionComponent } from 'react';
import { Alert, ScrollView, StyleSheet } from 'react-native';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';
import ApiUser from '_api/user.api';
import { showHttpClientErr } from '_config/httpClient.config';
import colors from '_constants/colors';
import defaultStyles from '_constants/styles';
import { signout } from '_store/auth/actions';
import { Button, Card, Screen, Text, TextInput } from '_tevno/index';

interface FormValues {
  password: string;
}

const validation = Yup.object({
  password: Yup.string().min(8).required(),
});

const UserDeletionScreen: FunctionComponent = () => {
  const dispatch = useDispatch();

  const onSubmit = async (values: FormValues) => {
    try {
      await ApiUser.delete(values);

      Alert.alert('Success !', `Your account has been deleted.`);

      dispatch(signout());
    } catch (err) {
      showHttpClientErr(err);
    }
  };

  return (
    <Formik onSubmit={onSubmit} initialValues={{ password: '' }} validationSchema={validation}>
      {({ handleChange, handleBlur, handleSubmit, isSubmitting, values, errors, touched }) => (
        <Screen>
          <ScrollView contentContainerStyle={styles.scrollView}>
            <Text style={defaultStyles.title}>My account</Text>
            <Text style={[defaultStyles.subTitle, styles.subTitle]}>Account deletion</Text>

            <Card>
              <Text
                style={styles.description}
              >{`Your are about to delete your account. When deleting your account, all your data are deleted !`}</Text>

              <TextInput
                style={styles.input}
                placeholder="Password"
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
                value={values.password}
                autoCompleteType="password"
                autoCorrect
                textContentType="password"
                autoCapitalize="none"
                secureTextEntry
                isInvalid={!!(touched.password && errors.password)}
              />

              <Button
                color="danger"
                block
                isPending={isSubmitting}
                onPress={() => handleSubmit()}
              >{`Delete my account`}</Button>
            </Card>
          </ScrollView>
        </Screen>
      )}
    </Formik>
  );
};

export default UserDeletionScreen;

const styles = StyleSheet.create({
  scrollView: {
    paddingHorizontal: 20,
  },
  subTitle: {
    color: colors.danger,
  },
  description: {
    color: colors.greyDark,
    marginBottom: 30,
  },
  input: {
    marginBottom: 15,
  },
});
