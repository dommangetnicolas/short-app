import { useNavigation } from '@react-navigation/core';
import React, { FunctionComponent } from 'react';
import HeaderButton from '_components/Navigation/HeaderButton';
import { useActions } from '_tevno/index';

const UserScreenHeader: FunctionComponent = () => {
  const { showActions } = useActions();
  const { navigate } = useNavigation();

  const onPress = () => {
    showActions([
      {
        title: 'Update my information',
        callback: () => navigate('UserEditScreen'),
      },
      {
        title: 'Delete my account',
        destructive: true,
        callback: () => navigate('UserDeletionScreen'),
      },
      {
        title: 'Cancel',
        cancel: true,
      },
    ]);
  };

  return <HeaderButton icon="chevron-down-outline" onPress={onPress} />;
};

export default UserScreenHeader;
