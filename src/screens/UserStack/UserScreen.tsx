import dayjs from 'dayjs';
import React, { FunctionComponent } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { useSelector } from 'react-redux';
import defaultStyles from '_constants/styles';
import { RootState } from '_store/index.d';
import { Card, Screen, Text } from '_tevno/index';

const UserScreen: FunctionComponent = () => {
  const { user } = useSelector((state: RootState) => state.User);

  if (!user) return null;

  return (
    <Screen>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <Text style={defaultStyles.title}>My account</Text>
        <Text style={defaultStyles.subTitle}>
          {user.firstName.capitalize()} {user.lastName.capitalize()}
        </Text>

        <Card>
          <View style={styles.row}>
            <Text style={defaultStyles.key}>First name</Text>
            <Text style={defaultStyles.value}>{user.firstName.capitalize()}</Text>
          </View>

          <View style={styles.row}>
            <Text style={defaultStyles.key}>Last name</Text>
            <Text style={defaultStyles.value}>{user.lastName.capitalize()}</Text>
          </View>

          <View style={styles.row}>
            <Text style={defaultStyles.key}>Email</Text>
            <Text style={defaultStyles.value}>{user.email}</Text>
          </View>

          <View>
            <Text style={defaultStyles.key}>Signed up on</Text>
            <Text style={defaultStyles.value}>{dayjs(user.createdAt).format('L')}</Text>
          </View>
        </Card>
      </ScrollView>
    </Screen>
  );
};

export default UserScreen;

const styles = StyleSheet.create({
  scrollView: {
    paddingHorizontal: 20,
  },
  row: {
    marginBottom: 10,
  },
});
