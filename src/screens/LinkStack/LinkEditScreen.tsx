import { RouteProp, useRoute } from '@react-navigation/core';
import { Formik } from 'formik';
import React, { FunctionComponent } from 'react';
import { Alert, ScrollView, StyleSheet } from 'react-native';
import ApiLink from '_api/link.api';
import { showHttpClientErr } from '_config/httpClient.config';
import defaultStyles from '_constants/styles';
import { Button, Card, Screen, Text, TextInput } from '_tevno/index';
import { LinkStackParams } from '_types/navigation/LinkStack';

interface FormValues {
  name: string;
  target: string;
  shortLink: string;
}

const LinkEditScreen: FunctionComponent = () => {
  const { params } = useRoute<RouteProp<LinkStackParams, 'LinkEditScreen'>>();
  const { data } = { ...params };

  if (!data) return null;

  const onSubmit = async (values: FormValues) => {
    try {
      await ApiLink.update(data._id, values);
      Alert.alert('Link updated', `${data.name} has been updated.`);
    } catch (err) {
      showHttpClientErr(err);
    }
  };

  return (
    <Formik onSubmit={onSubmit} initialValues={{ ...data }}>
      {({ handleChange, handleBlur, handleSubmit, isSubmitting, values, errors, touched }) => (
        <Screen>
          <ScrollView contentContainerStyle={styles.scrollView}>
            <Text style={defaultStyles.title}>Link edition</Text>
            <Text style={defaultStyles.subTitle}>{data.name.capitalize()}</Text>

            <Card>
              <TextInput
                style={styles.input}
                placeholder="Name"
                value={values.name}
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={handleChange('name')}
                onBlur={handleBlur('name')}
                isInvalid={!!(touched.name && errors.name)}
              />

              <TextInput
                style={styles.input}
                placeholder="Target link"
                value={values.target}
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={handleChange('target')}
                onBlur={handleBlur('target')}
                isInvalid={!!(touched.target && errors.target)}
              />

              <TextInput
                style={styles.input}
                placeholder="Short link"
                value={values.shortLink}
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={handleChange('shortLink')}
                onBlur={handleBlur('shortLink')}
                isInvalid={!!(touched.shortLink && errors.shortLink)}
              />

              <Button color="brand" block isPending={isSubmitting} onPress={() => handleSubmit()}>
                Edit link
              </Button>
            </Card>
          </ScrollView>
        </Screen>
      )}
    </Formik>
  );
};

export default LinkEditScreen;

const styles = StyleSheet.create({
  scrollView: {
    paddingHorizontal: 20,
  },
  input: {
    marginBottom: 15,
  },
});
