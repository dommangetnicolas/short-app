import { RouteProp, useNavigation, useRoute } from '@react-navigation/core';
import React, { FunctionComponent, useEffect, useLayoutEffect, useState } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import ApiLink from '_api/link.api';
import { showHttpClientErr } from '_config/httpClient.config';
import defaultStyles from '_constants/styles';
import { Card, Screen, Text } from '_tevno/index';
import { ILinkModel } from '_types/models/LinkModel/ILinkModel';
import { LinkStackParams } from '_types/navigation/LinkStack';
import LinkScreenHeader from './LinkScreenHeader';

const LinkScreen: FunctionComponent = () => {
  const [data, setData] = useState<ILinkModel | null>(null);

  const { setOptions, addListener } = useNavigation();
  const { params } = useRoute<RouteProp<LinkStackParams, 'LinkScreen'>>();

  const fetch = async (_id: string) => {
    try {
      const { data: dataFromReq } = await ApiLink.get(_id, { populate: { domain: true } });

      setData(dataFromReq);
    } catch (err) {
      showHttpClientErr(err);
    }
  };

  useLayoutEffect(() => {
    if (data) {
      setOptions({
        headerRight: () => <LinkScreenHeader link={data} />,
      });
    }
  }, [data, setOptions]);

  useEffect(() => {
    if (params._id) {
      fetch(params._id);
    }
  }, [params._id]);

  useEffect(() => {
    const unsubscribe = addListener('focus', () => {
      if (params._id) {
        fetch(params._id);
      }
    });

    return unsubscribe;
  }, [addListener, params._id]);

  if (!data) return null;

  return (
    <Screen>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <Text style={defaultStyles.title}>Link</Text>
        <Text style={defaultStyles.subTitle}>{data.name.capitalize()}</Text>

        <Card>
          <View style={styles.row}>
            <Text style={defaultStyles.key}>Name</Text>
            <Text style={defaultStyles.value}>{data.name.capitalize()}</Text>
          </View>

          <View style={styles.row}>
            <Text style={defaultStyles.key}>Domain</Text>
            <Text style={defaultStyles.value}>{data.domain?.domain?.capitalize?.()}</Text>
          </View>

          <View style={styles.row}>
            <Text style={defaultStyles.key}>ShortLink</Text>
            <Text style={defaultStyles.value}>{data.shortLink}</Text>
          </View>

          <View>
            <Text style={defaultStyles.key}>Target</Text>
            <Text style={defaultStyles.value}>{data.target}</Text>
          </View>
        </Card>
      </ScrollView>
    </Screen>
  );
};

export default LinkScreen;

const styles = StyleSheet.create({
  scrollView: {
    paddingHorizontal: 20,
  },
  row: {
    marginBottom: 10,
  },
});
