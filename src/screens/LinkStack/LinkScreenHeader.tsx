import { useNavigation } from '@react-navigation/core';
import React, { FunctionComponent } from 'react';
import { Alert } from 'react-native';
import ApiLink from '_api/link.api';
import HeaderButton from '_components/Navigation/HeaderButton';
import { showHttpClientErr } from '_config/httpClient.config';
import { useActions } from '_tevno/index';
import { ILinkModel } from '_types/models/LinkModel/ILinkModel';

interface IProps {
  link: ILinkModel;
}

const LinkScreenHeader: FunctionComponent<IProps> = (props) => {
  const { link } = props;

  const { showActions } = useActions();
  const { goBack, navigate } = useNavigation();

  const deleteHandler = async () => {
    try {
      await ApiLink.delete(link._id);

      Alert.alert('Link deleted', `${link.name} has been deleted.`);
      goBack();
    } catch (err) {
      showHttpClientErr(err);
    }
  };

  const onDelete = () => {
    showActions([
      {
        title: `Delete ${link.name}`,
        destructive: true,
        callback: deleteHandler,
      },
      {
        title: 'Cancel',
        cancel: true,
      },
    ]);
  };

  const onPress = () => {
    showActions([
      {
        title: 'Edit',
        callback: () => navigate('LinkEditScreen', { data: link }),
      },
      {
        title: 'Delete',
        destructive: true,
        callback: onDelete,
      },
      {
        title: 'Cancel',
        cancel: true,
      },
    ]);
  };

  return <HeaderButton icon="chevron-down-outline" onPress={onPress} />;
};

export default LinkScreenHeader;
