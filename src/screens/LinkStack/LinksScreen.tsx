import React, { FunctionComponent } from 'react';
import { StyleSheet, View } from 'react-native';
import ApiLink from '_api/link.api';
import LinkOverview from '_components/LinkStack/LinkOverview';
import QuickCreateLink from '_components/LinkStack/QuickCreateLink';
import defaultStyles from '_constants/styles';
import { List, Screen, Text } from '_tevno/index';

const LinksScreen: FunctionComponent = () => {
  const fetch = (page: number) => {
    return ApiLink.list({
      page: {
        index: page,
        size: 20,
      },
      sort: {
        createdAt: -1,
      },
      populate: {
        domain: true,
      },
    });
  };

  return (
    <Screen>
      <List
        fetch={fetch}
        ListHeaderComponent={() => (
          <View style={styles.header}>
            <Text style={defaultStyles.title}>Links</Text>
            <QuickCreateLink />
          </View>
        )}
        renderItem={({ item }) => <LinkOverview item={item} />}
      />
    </Screen>
  );
};

export default LinksScreen;

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: 20,
    marginBottom: 10,
  },
});
