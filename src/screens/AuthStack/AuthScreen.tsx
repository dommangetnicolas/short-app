import { useNavigation } from '@react-navigation/core';
import React, { FunctionComponent } from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import LogoWhiteSvg from '_assets/images/LogoWhiteSvg';
import colors from '_constants/colors';
import fonts from '_constants/fonts';
import gradients from '_constants/gradients';
import { Button, FocusAwareStatusBar, Text } from '_tevno/index';

const AuthScreen: FunctionComponent = () => {
  const { navigate } = useNavigation();

  return (
    <View style={styles.container}>
      <FocusAwareStatusBar barStyle="light-content" backgroundColor="transparent" />

      <LinearGradient colors={gradients.primary} style={styles.background}>
        <LogoWhiteSvg height={128} width={128} />
      </LinearGradient>

      <View style={styles.contentContainer}>
        <View style={styles.textContainer}>
          <Text style={styles.title}>Welcome to</Text>
          <Text style={[styles.title, styles.subTitle]}>Short</Text>
          <Text style={styles.description}>A simple link shortener.</Text>
        </View>

        <View style={styles.buttonContainer}>
          <Button
            color="brand"
            style={[styles.button, styles.buttonLeft]}
            onPress={() => navigate('SigninScreen')}
            size="lg"
          >
            Sign in
          </Button>
          <Button
            color="brand"
            outline
            style={[styles.button, styles.buttonRight]}
            onPress={() => navigate('SignupScreen')}
            size="lg"
          >
            Sign up
          </Button>
        </View>
      </View>
    </View>
  );
};

export default AuthScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    height: Dimensions.get('window').height * 0.55,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: Dimensions.get('window').height * 0.5,
    backgroundColor: colors.white,

    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    padding: 40,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  textContainer: {
    flex: 1,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  title: {
    color: colors.greyDarker,
    fontFamily: fonts.primaryBold,
    fontSize: 45,
    marginBottom: 0,
  },
  subTitle: {
    color: colors.brand,
    marginBottom: 15,
  },
  description: {
    color: colors.greyDark,
    fontSize: 20,
  },
  button: {
    flex: 1,
    alignSelf: undefined,
  },
  buttonLeft: {
    marginRight: 5,
  },
  buttonRight: {
    marginLeft: 5,
  },
});
