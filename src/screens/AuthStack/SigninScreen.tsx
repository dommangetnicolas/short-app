import { useNavigation } from '@react-navigation/core';
import { Formik } from 'formik';
import React, { FunctionComponent, useRef } from 'react';
import {
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';
import ApiAuth from '_api/auth.api';
import LogoSvg from '_assets/images/LogoSvg';
import { showHttpClientErr } from '_config/httpClient.config';
import colors from '_constants/colors';
import fonts from '_constants/fonts';
import { setToken } from '_store/auth/actions';
import { Button, Screen, Text, TextInput } from '_tevno/index';

interface FormValues {
  email: string;
  password: string;
}

const validation = Yup.object({
  email: Yup.string().email().required(),
  password: Yup.string().min(8).required(),
});

const SigninScreen: FunctionComponent = () => {
  const passwordRef = useRef<any>();

  const dispatch = useDispatch();
  const { navigate } = useNavigation();

  const onSubmit = async (values: FormValues) => {
    try {
      const { data } = await ApiAuth.signIn(values.email, values.password);

      if (data.token) {
        dispatch(setToken(data.token));
      }
    } catch (err) {
      showHttpClientErr(err);
    }
  };

  return (
    <Screen style={styles.screen}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <KeyboardAvoidingView
          style={styles.container}
          keyboardVerticalOffset={75}
          {...(Platform.OS === 'ios' && { behavior: 'padding' })}
        >
          <Formik
            onSubmit={onSubmit}
            initialValues={{
              email: '',
              password: '',
            }}
            validationSchema={validation}
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              values,
              errors,
              touched,
            }) => (
              <View>
                <View style={styles.headerContainer}>
                  <LogoSvg style={styles.logo} height={128} width={128} />
                  <Text style={styles.title}>Welcome back !</Text>
                  <Text style={styles.description}>
                    Nice to see you again, let's go for shortening links.
                  </Text>
                </View>

                <TextInput
                  style={styles.input}
                  placeholder="Email"
                  onChangeText={handleChange('email')}
                  onBlur={handleBlur('email')}
                  value={values.email}
                  autoCompleteType="email"
                  autoCorrect={false}
                  keyboardType="email-address"
                  autoCapitalize="none"
                  returnKeyType="next"
                  textContentType="username"
                  onSubmitEditing={() => passwordRef?.current?.focus?.()}
                  {...(touched.email && errors.email && { isInvalid: true })}
                />

                <TextInput
                  style={styles.input}
                  ref={passwordRef}
                  placeholder="Mot de passe"
                  onChangeText={handleChange('password')}
                  onBlur={handleBlur('password')}
                  value={values.password}
                  autoCompleteType="password"
                  autoCorrect
                  textContentType="password"
                  autoCapitalize="none"
                  secureTextEntry
                  isPassword
                  {...(touched.password && errors.password && { isInvalid: true })}
                />

                <Button
                  color="brand"
                  isPending={isSubmitting}
                  onPress={() => handleSubmit()}
                  block
                  size="lg"
                >
                  Sign in
                </Button>

                <View style={styles.signupText}>
                  <Text style={styles.signupQuestion}>Don't have an account ?</Text>
                  <TouchableOpacity
                    style={styles.signupButton}
                    onPress={() => navigate('SignupScreen')}
                  >
                    <Text>Signup</Text>
                  </TouchableOpacity>
                </View>
              </View>
            )}
          </Formik>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    </Screen>
  );
};

export default SigninScreen;

const styles = StyleSheet.create({
  screen: {
    paddingHorizontal: 20,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    alignItems: 'center',
    paddingVertical: 50,
  },
  logo: {
    marginBottom: 30,
  },
  title: {
    color: colors.greyDarker,
    fontFamily: fonts.primaryBold,
    fontSize: 45,
    marginBottom: 15,
  },
  description: {
    color: colors.greyDark,
    fontSize: 20,
    textAlign: 'center',
  },
  input: {
    marginBottom: 15,
  },
  signupText: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  signupQuestion: {
    color: colors.greyDark,
  },
  signupButton: {
    padding: 5,
  },
});
