import { Formik } from 'formik';
import React, { FunctionComponent, useRef } from 'react';
import {
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';
import ApiAuth from '_api/auth.api';
import LogoSvg from '_assets/images/LogoSvg';
import { showHttpClientErr } from '_config/httpClient.config';
import colors from '_constants/colors';
import fonts from '_constants/fonts';
import { setToken } from '_store/auth/actions';
import { Button, Screen, Text, TextInput } from '_tevno/index';

interface FormValues {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

const validation = Yup.object({
  firstName: Yup.string().min(1).required(),
  lastName: Yup.string().min(1).required(),
  email: Yup.string().email().required(),
  password: Yup.string().min(8).required(),
});

const SignupScreen: FunctionComponent = () => {
  const lastNameRef = useRef<any>();
  const emailRef = useRef<any>();
  const passwordRef = useRef<any>();

  const dispatch = useDispatch();

  const onSubmit = async (values: FormValues) => {
    try {
      const { data } = await ApiAuth.signUp(values);

      if (data.token) {
        dispatch(setToken(data.token));
      }
    } catch (err) {
      showHttpClientErr(err);
    }
  };

  return (
    <Screen style={styles.screen}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <KeyboardAvoidingView
          style={styles.container}
          keyboardVerticalOffset={300}
          {...(Platform.OS === 'ios' && { behavior: 'padding' })}
        >
          <Formik
            onSubmit={onSubmit}
            initialValues={{
              firstName: '',
              lastName: '',
              email: '',
              password: '',
            }}
            validationSchema={validation}
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              values,
              errors,
              touched,
            }) => (
              <View>
                <View style={styles.headerContainer}>
                  <LogoSvg style={styles.logo} height={128} width={128} />
                  <Text style={styles.title}>Welcome !</Text>
                  <Text style={styles.description}>Sign up and let's go to shorten links.</Text>
                </View>

                <TextInput
                  style={styles.input}
                  placeholder="First name"
                  onChangeText={handleChange('firstName')}
                  onBlur={handleBlur('firstName')}
                  value={values.firstName}
                  autoCorrect={false}
                  returnKeyType="next"
                  blurOnSubmit={false}
                  onSubmitEditing={() => lastNameRef.current.focus()}
                  isInvalid={!!(touched.firstName && errors.firstName)}
                />

                <TextInput
                  ref={lastNameRef}
                  style={styles.input}
                  placeholder="Last name"
                  onChangeText={handleChange('lastName')}
                  onBlur={handleBlur('lastName')}
                  value={values.lastName}
                  autoCorrect={false}
                  returnKeyType="next"
                  blurOnSubmit={false}
                  onSubmitEditing={() => emailRef.current.focus()}
                  isInvalid={!!(touched.lastName && errors.lastName)}
                />

                <TextInput
                  ref={emailRef}
                  style={styles.input}
                  placeholder="Email"
                  onChangeText={handleChange('email')}
                  onBlur={handleBlur('email')}
                  value={values.email}
                  autoCompleteType="email"
                  autoCorrect={false}
                  keyboardType="email-address"
                  autoCapitalize="none"
                  returnKeyType="next"
                  textContentType="username"
                  onSubmitEditing={() => passwordRef?.current?.focus?.()}
                  isInvalid={!!(touched.email && errors.email)}
                />

                <TextInput
                  style={styles.input}
                  ref={passwordRef}
                  placeholder="Password"
                  onChangeText={handleChange('password')}
                  onBlur={handleBlur('password')}
                  value={values.password}
                  autoCompleteType="password"
                  autoCorrect
                  textContentType="password"
                  autoCapitalize="none"
                  secureTextEntry
                  isPassword
                  isInvalid={!!(touched.password && errors.password)}
                />

                <Button block color="brand" isPending={isSubmitting} onPress={() => handleSubmit()}>
                  Sign up
                </Button>
              </View>
            )}
          </Formik>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    </Screen>
  );
};

export default SignupScreen;

const styles = StyleSheet.create({
  screen: {
    paddingHorizontal: 20,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    alignItems: 'center',
    paddingVertical: 50,
  },
  logo: {
    marginBottom: 30,
  },
  title: {
    color: colors.greyDarker,
    fontFamily: fonts.primaryBold,
    fontSize: 45,
    marginBottom: 15,
  },
  description: {
    color: colors.greyDark,
    fontSize: 20,
    textAlign: 'center',
  },
  input: {
    marginBottom: 15,
  },
});
