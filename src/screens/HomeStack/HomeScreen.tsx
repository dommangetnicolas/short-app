import React, { FunctionComponent } from 'react';
import { ScrollView } from 'react-native';
import { useSelector } from 'react-redux';
import QuickCreateLink from '_components/LinkStack/QuickCreateLink';
import defaultStyles from '_constants/styles';
import { RootState } from '_store/index.d';
import { Screen, Text } from '_tevno/index';

const HomeScreen: FunctionComponent = () => {
  const { user } = useSelector((state: RootState) => state.User);

  return (
    <Screen>
      <ScrollView contentContainerStyle={defaultStyles.scrollWiew}>
        <Text style={defaultStyles.title}>Home</Text>
        {user?.firstName && user?.firstName?.length > 0 && (
          <Text style={defaultStyles.subTitle}>Hello {user.firstName.capitalize()} 👋</Text>
        )}
        <QuickCreateLink />
      </ScrollView>
    </Screen>
  );
};

export default HomeScreen;
