import React, { FunctionComponent } from 'react';
import { StyleSheet, View } from 'react-native';
import ApiDomain from '_api/domain.api';
import DomainOverview from '_components/DomainStack/DomainOverview';
import defaultStyles from '_constants/styles';
import { List, Screen, Text } from '_tevno/index';
import { IDomainModel } from '_types/models/DomainModel/IDomainModel';

const DomainsScreen: FunctionComponent = () => {
  const fetch = (page: number) => {
    return ApiDomain.list({
      page: {
        index: page,
        size: 20,
      },
      sort: {
        createdAt: -1,
      },
    });
  };

  return (
    <Screen>
      <List<IDomainModel>
        fetch={fetch}
        ListHeaderComponent={() => (
          <View style={styles.header}>
            <Text style={defaultStyles.title}>Domains</Text>
          </View>
        )}
        renderItem={({ item }) => <DomainOverview item={item} />}
      />
    </Screen>
  );
};

export default DomainsScreen;

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: 20,
  },
});
