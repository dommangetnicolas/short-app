import { useNavigation } from '@react-navigation/core';
import { Formik } from 'formik';
import React, { FunctionComponent } from 'react';
import { Alert, ScrollView, StyleSheet, View } from 'react-native';
import * as Yup from 'yup';
import ApiDomain from '_api/domain.api';
import { showHttpClientErr } from '_config/httpClient.config';
import { Button, Screen, Text, TextInput } from '_tevno/index';

interface FormValues {
  domain: string;
}

const validation = Yup.object({
  domain: Yup.string().domain().required(),
});

const DomainCreateScreen: FunctionComponent = () => {
  const { goBack } = useNavigation();

  const onSubmit = async (values: FormValues) => {
    try {
      const { data } = await ApiDomain.create(values);

      Alert.alert('Domain created', `${data.domain} has been created.`);
      goBack();
    } catch (err) {
      showHttpClientErr(err);
    }
  };

  return (
    <Formik onSubmit={onSubmit} initialValues={{ domain: '' }} validationSchema={validation}>
      {({ handleChange, handleBlur, handleSubmit, isSubmitting, values, errors, touched }) => (
        <Screen>
          <ScrollView contentContainerStyle={styles.scrollView}>
            <Text>{`Your are about to add a domain. When adding a domain, be sure you are the owner and able to modify DNS records !`}</Text>

            <View style={styles.form}>
              <Text>Domain:</Text>
              <TextInput
                style={styles.input}
                placeholder="Domain"
                value={values.domain}
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={handleChange('domain')}
                onBlur={handleBlur('domain')}
                isInvalid={!!(touched.domain && errors.domain)}
              />

              <Button
                color="brand"
                isPending={isSubmitting}
                onPress={() => handleSubmit()}
                block
              >{`Create ${values.domain}`}</Button>
            </View>
          </ScrollView>
        </Screen>
      )}
    </Formik>
  );
};

export default DomainCreateScreen;

const styles = StyleSheet.create({
  scrollView: {
    paddingHorizontal: 20,
  },
  form: {
    marginTop: 30,
  },
  input: {
    marginBottom: 5,
  },
});
