import { RouteProp, useNavigation, useRoute } from '@react-navigation/core';
import dayjs from 'dayjs';
import { Formik } from 'formik';
import React, { FunctionComponent } from 'react';
import { Alert, ScrollView, StyleSheet, View } from 'react-native';
import * as Yup from 'yup';
import ApiDomain from '_api/domain.api';
import { showHttpClientErr } from '_config/httpClient.config';
import colors from '_constants/colors';
import defaultStyles from '_constants/styles';
import { Button, Card, Screen, Text, TextInput } from '_tevno/index';
import { DomainStackParams } from '_types/navigation/DomainStack';

interface FormValues {
  domain: string;
}

const DomainDeletionScreen: FunctionComponent = () => {
  const { params } = useRoute<RouteProp<DomainStackParams, 'DomainDeletionScreen'>>();
  const { domain } = { ...params };

  const { goBack } = useNavigation();

  const onSubmit = async (values: FormValues) => {
    try {
      if (!domain?._id) {
        return;
      }

      await ApiDomain.delete(domain?._id, values);

      Alert.alert('Success !', `${domain.domain} has been deleted.`);
      goBack();
    } catch (err) {
      showHttpClientErr(err);
    }
  };

  if (!domain) return null;

  const validation = Yup.object({
    domain: Yup.string().equal(domain.domain).required(),
  });

  return (
    <Formik onSubmit={onSubmit} initialValues={{ domain: '' }} validationSchema={validation}>
      {({ handleChange, handleBlur, handleSubmit, isSubmitting, values, errors, touched }) => (
        <Screen>
          <ScrollView contentContainerStyle={styles.scrollView}>
            <Text style={defaultStyles.title}>Domain</Text>
            <Text style={[defaultStyles.subTitle, styles.subTitle]}>Delete {domain.domain}</Text>

            <Card>
              <View style={styles.row}>
                <Text style={defaultStyles.key}>Domain</Text>
                <Text style={defaultStyles.value}>{domain.domain}</Text>
              </View>

              <View style={styles.row}>
                <Text style={defaultStyles.key}>Created on</Text>
                <Text style={defaultStyles.value}>{dayjs(domain.createdAt).format('L')}</Text>
              </View>

              <Text
                style={styles.description}
              >{`Your are about to delete ${domain.domain}. When deleting a domain, all of its links are deleted !`}</Text>

              <TextInput
                style={styles.input}
                placeholder="Domain name"
                value={values.domain}
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={handleChange('domain')}
                onBlur={handleBlur('domain')}
                isInvalid={!!(touched.domain && errors.domain)}
              />

              <Button
                color="danger"
                style={styles.confirmButton}
                block
                isPending={isSubmitting}
                onPress={() => handleSubmit()}
              >{`Delete ${domain.domain}`}</Button>
            </Card>
          </ScrollView>
        </Screen>
      )}
    </Formik>
  );
};

export default DomainDeletionScreen;

const styles = StyleSheet.create({
  scrollView: {
    paddingHorizontal: 20,
  },
  subTitle: {
    color: colors.danger,
  },
  row: {
    marginBottom: 10,
  },
  description: {
    color: colors.greyDark,
    marginBottom: 30,
  },
  input: {
    marginBottom: 15,
  },
});
