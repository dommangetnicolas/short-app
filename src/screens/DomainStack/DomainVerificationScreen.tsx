import { RouteProp, useNavigation, useRoute } from '@react-navigation/core';
import React, { FunctionComponent, useEffect, useState } from 'react';
import { Alert, ScrollView, StyleSheet, View } from 'react-native';
import ApiDomainVerification from '_api/domainVerification.api';
import { showHttpClientErr } from '_config/httpClient.config';
import colors from '_constants/colors';
import defaultStyles from '_constants/styles';
import { Button, Card, Screen, Text } from '_tevno/index';
import { IDomainVerificationModel } from '_types/models/DomainVerificationModel/IDomainVerificationModel';
import { DomainStackParams } from '_types/navigation/DomainStack';

const DomainVerificationScreen: FunctionComponent = () => {
  const [isPending, setIsPending] = useState<boolean>(false);
  const [data, setData] = useState<IDomainVerificationModel | null>(null);

  const { addListener, goBack } = useNavigation();
  const { params } = useRoute<RouteProp<DomainStackParams, 'DomainVerificationScreen'>>();
  const { domain } = { ...params };

  const fetch = async (_id: string) => {
    try {
      const { data: dataFromReq } = await ApiDomainVerification.get(_id);

      setData(dataFromReq);
    } catch (err) {
      showHttpClientErr(err);
    }
  };

  useEffect(() => {
    if (domain?._id) {
      fetch(domain._id);
    }
  }, [domain?._id]);

  useEffect(() => {
    const unsubscribe = addListener('focus', () => {
      if (domain?._id) {
        fetch(domain._id);
      }
    });

    return unsubscribe;
  }, [addListener, domain?._id]);

  if (!domain || !data) return null;

  const onSubmit = async () => {
    try {
      setIsPending(true);
      await ApiDomainVerification.verify(domain._id);

      Alert.alert('Success !', `${domain.domain} has been verified.`);

      goBack();
    } catch (err) {
      showHttpClientErr(err);
    } finally {
      setIsPending(false);
    }
  };

  return (
    <Screen>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <Text style={defaultStyles.title}>Domain verification</Text>
        <Text style={defaultStyles.subTitle}>{domain.domain.capitalize()}</Text>

        <Card>
          <Text style={styles.infoText}>
            Your domain has been created with short. Small steps are remaining:
          </Text>

          <View style={styles.row}>
            <Text style={defaultStyles.key}>Step 1</Text>
            <Text
              style={defaultStyles.value}
            >{`Step 1: Create ${domain.domain} A record point to 135.125.181.181`}</Text>
          </View>

          <View style={styles.row}>
            <Text style={defaultStyles.key}>Step 2</Text>
            <Text
              style={defaultStyles.value}
            >{`Create ${domain.domain} TXT record with this value: ${data.uuid}`}</Text>
          </View>

          <View style={styles.row}>
            <Text style={defaultStyles.key}>Step 3</Text>
            <Text
              style={defaultStyles.value}
            >{`Once previous steps are done, click on the button below`}</Text>
          </View>

          <Button
            color="brand"
            style={styles.confirmButton}
            block
            isPending={isPending}
            onPress={onSubmit}
          >{`Verify ${domain.domain}`}</Button>

          <Text style={styles.noteText}>
            Note 1: It is possible that the records take time to propagate across DNS servers.
          </Text>
          <Text style={styles.noteText}>
            {`Note 2: Once your domain is verified you can delete TXT record. (Be careful to let the A record)`}
          </Text>
        </Card>
      </ScrollView>
    </Screen>
  );
};

export default DomainVerificationScreen;

const styles = StyleSheet.create({
  scrollView: {
    paddingHorizontal: 20,
  },
  infoText: {
    color: colors.brand,
    marginBottom: 10,
  },
  row: {
    marginBottom: 10,
  },
  confirmButton: {
    marginTop: 20,
  },
  noteText: {
    marginTop: 10,
    color: colors.greyDark,
  },
});
